#ifndef _VARS_
#define _VARS_

#define APP_NAME "Gordon Reversing - Tool - powered by GFramework"
#define W_NAME_TASKMGR "Task Manager"
#define W_NAME_MEMSEARCHER "Memory Searcher"
#define TASKMGR_REFRESH_INTERVAL 500

#define CONNECTBUTTON(parent, id, func) parent->Connect(id, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(func));
#define CONNECTMENUITEM(parent, id, func) parent->Connect(id, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(func));
#define CONNECTCHECKBOX(parent, id, func) parent->Connect(id, wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler(func));

#define LOOP_LISTCTRL_SELECTED(code) { int i = -1; while ((i = listCtrl->GetNextItem(i, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)) != -1) { code } }
#define LOOP_LISTCTRL_DONTCARE(code) { int i = -1; while ((i = listCtrl->GetNextItem(i, wxLIST_NEXT_ALL, wxLIST_STATE_DONTCARE)) != -1) { code } }

#define LOOP_LISTCTRL_SELECTED_P(parent, code) { int i = -1; while ((i = parent->GetNextItem(i, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)) != -1) { code } } 
#define LOOP_LISTCTRL_DONTCARE_P(parent, code) { int i = -1; while ((i = parent->GetNextItem(i, wxLIST_NEXT_ALL, wxLIST_STATE_DONTCARE)) != -1) { code } }

#define LISTITEM_SELECTED() listCtrl->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)
#define LISTITEM_SELECTED_P(parent) parent->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED)

#endif