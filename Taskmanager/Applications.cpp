#include "Applications.h"

namespace Applications {

	wxListCtrl *listCtrl;
	wxSearchCtrl *searchCtrl;
	wxImageList *imageList;

	WindowEnum *we;
	HANDLE handle_timer = NULL;

	Applications::Applications(wxWindow *win, wxWindowID winid) : wxPanel(win, winid)
	{
		we = new WindowEnum;

		imageList = new wxImageList(16, 16);

		listCtrl = new wxListCtrl(this, ID_LISTCTRL, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_SORT_ASCENDING | wxLC_VRULES);

		int c1 = listCtrl->InsertColumn(0, wxT("Task"));
		int c2 = listCtrl->InsertColumn(1, wxT("Handle"), wxLIST_FORMAT_RIGHT);
		int c3 = listCtrl->InsertColumn(2, wxT("Class"));
		int c4 = listCtrl->InsertColumn(3, wxT("PID"), wxLIST_FORMAT_RIGHT);
		int c5 = listCtrl->InsertColumn(4, wxT("Status"));

		listCtrl->SetColumnWidth(c1, 330);
		listCtrl->SetColumnWidth(c2, 70);
		listCtrl->SetColumnWidth(c3, 185);

		listCtrl->Connect(ID_LISTCTRL, wxEVT_CONTEXT_MENU, wxCommandEventHandler(Applications::OnContextMenuEvent));

		wxButton *b3 = new wxButton(this, ID_BUTTON_NEWTASK, wxT("New Task..."), wxDefaultPosition, wxSize(-1, 23));

		CONNECTBUTTON(this, ID_BUTTON_NEWTASK, Applications::OnButtonEvent);

		listCtrl->Bind(wxEVT_CHAR, [](wxKeyEvent &event) {
			int key = event.GetKeyCode();

			if (key == 127) {
				wxContextMenuEvent event(wxEVT_CONTEXT_MENU, ID_CONTEXTMENU_DESTROY);
				reinterpret_cast<Applications*>(Taskmanager::panel.applications)->OnContextMenuEvent(event);
			}
		});

		searchCtrl = new wxSearchCtrl(this, ID_SEARCHCTRL);
		searchCtrl->ShowSearchButton(false);
		searchCtrl->SetDescriptiveText(wxT("Filter..."));

		searchCtrl->Connect(ID_SEARCHCTRL, wxEVT_TEXT, wxCommandEventHandler(Applications::OnSearchCtrlEvent));
		searchCtrl->SetFocus();

		wxBoxSizer *main = new wxBoxSizer(wxVERTICAL);
		wxBoxSizer *bar = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *bar_left = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *bar_right = new wxBoxSizer(wxHORIZONTAL);

		bar_left->Add(searchCtrl, 1, wxLEFT | wxRIGHT, 5);
		bar_right->Add(b3, 0, wxRIGHT, 5);

		bar->Add(bar_left, 50);
		bar->Add(bar_right, 1, wxRIGHT, 5);

		main->Add(listCtrl, 1, wxGROW | wxALL, 5);
		main->Add(bar);

		SetSizerAndFit(main);

		handle_timer = ThreadManager::CreateThread(TimerThread);

		listCtrl->Bind(wxEVT_SIZE, [](wxSizeEvent &event) {

			wxSize size = listCtrl->GetClientSize();
			float width = float(size.GetWidth()) / 100;

			listCtrl->Freeze();

			listCtrl->SetColumnWidth(0, width*45);
			listCtrl->SetColumnWidth(1, width*10);
			listCtrl->SetColumnWidth(2, width*23);
			listCtrl->SetColumnWidth(3, width*10);
			listCtrl->SetColumnWidth(4, width*12);

			listCtrl->Thaw();
		});

		//listCtrl->Bind(wxEVT_PAINT, [](wxPaintEvent &event) {
		//	LOOP_LISTCTRL_DONTCARE({
		//		wxRect rect_listctrl = listCtrl->GetRect();
		//		wxPoint pt_item;
		//		listCtrl->GetItemPosition(i, pt_item);

		//		if (pt_item.y < rect_listctrl.height)
		//		listCtrl->RefreshItem(i);
		//		});
		//});
	}

	Applications::~Applications()
	{
		TerminateThread(handle_timer, 0);
		CloseHandle(handle_timer);

		delete we;
	}

	void Applications::ShowContextMenu()
	{
		wxMenu *m = new wxMenu();
		wxMenuItem *switch_ = new wxMenuItem(m, ID_CONTEXTMENU_SWITCH, wxT("Switch to"));
		wxMenuItem *foreground = new wxMenuItem(m, ID_CONTEXTMENU_FOREGROUND, wxT("Foreground"));
		wxMenuItem *min = new wxMenuItem(m, ID_CONTEXTMENU_MINIMIZE, wxT("Minimize"));
		wxMenuItem *max = new wxMenuItem(m, ID_CONTEXTMENU_MAXIMIZE, wxT("Maximize"));
		wxMenuItem *restore = new wxMenuItem(m, ID_CONTEXTMENU_RESTORE, wxT("Restore"));
		wxMenuItem *destroy = new wxMenuItem(m, ID_CONTEXTMENU_DESTROY, wxT("Destroy"));
		wxMenuItem *proc = new wxMenuItem(m, ID_CONTEXTMENU_SHOWPROCESSES, wxT("Show process"));
		wxMenuItem *prop = new wxMenuItem(m, ID_CONTEXTMENU_PROPERTIES, wxT("Properties"));

		m->Append(switch_);
		m->Append(foreground);
		m->AppendSeparator();
		m->Append(min);
		m->Append(max);
		m->Append(restore);
		m->AppendSeparator();
		m->Append(destroy);
		m->Append(proc);
		m->AppendSeparator();
		m->Append(prop);

		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_SWITCH, Applications::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_FOREGROUND, Applications::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_MINIMIZE, Applications::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_MAXIMIZE, Applications::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_RESTORE, Applications::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_DESTROY, Applications::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_SHOWPROCESSES, Applications::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_PROPERTIES, Applications::OnContextMenuEvent);

		PopupMenu(m);
	}

	void Applications::NewTask(wxString cmdline)
	{
		if (cmdline.length()) {
			//TODO: exec 'cmdline' - find a better way
			wxSystem(wxString::Format(wxT("start %s"), cmdline));

			return;
		}

		TaskWindow* t = new TaskWindow();
		t->Show();
	}

	void Applications::Updates()
	{
		we->Clear();

		WindowList updated = we->Added();

		if (!we->HasUpdated(updated))
			return;

		WindowManager::FreeList(updated);

		UpdateListCtrl(we->List());
	}

	void Applications::AddEntries(WindowList wl)
	{
		wxString filter = searchCtrl->GetValue();

		int z = 0;
		for (WindowListIterator it = wl.begin(); it != wl.end(); it++) {

			wxString s = (*it)->Title();

			if (ProcessManager::RunningAsAdmin((*it)->ProcessId()))
				s.Append(L"+");

			if ((*it)->Wow64())
				s.Append(wxT("*"));

			wxString strwindowname = s;
			wxString strclassname = (*it)->Class();

			filter.MakeLower();
			strwindowname.MakeLower();
			strclassname.MakeLower();

			if (filter.length()) {
				if (!strwindowname.Contains(filter))
					if (!strclassname.Contains(filter))
						continue;
			}

			int image_index = 0;
			HICON handle_icon = (*it)->Icon();
			if (handle_icon) {
				wxIcon icon;
				icon.SetHICON(handle_icon);
				image_index = imageList->Add(icon);
				DestroyIcon(handle_icon);
			}
			else {
				image_index = imageList->Add(wxIcon(wxT("APPICON")));
			}

			int i = listCtrl->InsertItem(0, s, image_index);

			listCtrl->SetItem(i, 1, wxString::Format(wxT("%X"), (*it)->Handle()));
			listCtrl->SetItem(i, 2, (*it)->Class());
			listCtrl->SetItem(i, 3, wxString::Format(wxT("%d"), (*it)->ProcessId()));
			listCtrl->SetItem(i, 4, (*it)->Hung() ? wxT("Not responding") : wxT("Running"));

			if ((*it)->Hung())
				listCtrl->SetItemBackgroundColour(i, wxColour(255, 0, 0));
			else
				listCtrl->SetItemBackgroundColour(i, wxColour(255, 255, 255));

			listCtrl->SetItemData(i, (long)(*it)->Handle());

			wxIdleEvent idle;
			listCtrl->SendIdleEvents(idle);
		}

		listCtrl->SetImageList(imageList, wxIMAGE_LIST_SMALL);
	}

	void Applications::RemoveEntries(WindowList wl)
	{
		for (auto &it : wl) {
			LOOP_LISTCTRL_DONTCARE({
				HWND hwnd = (HWND)listCtrl->GetItemData(i);

				if (hwnd == it->Handle())
					listCtrl->DeleteItem(i);
			});
		}
	}

	void Applications::UpdateEntries()
	{
		LOOP_LISTCTRL_DONTCARE({
			wxRect rect_listctrl = listCtrl->GetRect();
			wxPoint pt_item;
			listCtrl->GetItemPosition(i, pt_item);

			if (pt_item.y > rect_listctrl.height)
				continue;

			wxString strTask = listCtrl->GetItemText(i, 0);
			wxString strHandle = listCtrl->GetItemText(i, 1);
			wxString strClass = listCtrl->GetItemText(i, 2);
			wxString strStatus = listCtrl->GetItemText(i, 4);

			HWND hwnd = (HWND)listCtrl->GetItemData(i);

			if (!WindowManager::IsValid(hwnd)) {
				listCtrl->DeleteItem(i);
				return;
			}

			Window *w = WindowManager::GetByHwnd(hwnd);
			if (w) {
				wxString newTask = w->Title();
				wxString newClass = w->Class();
				wxString newStatus = w->Hung() ? wxT("Not responding") : wxT("Running");

				if (w->Wow64())
					newTask.Append("*");

				if (ProcessManager::RunningAsAdmin(w->ProcessId()))
					newTask.Append(L"+");

				if (newTask != strTask)
					listCtrl->SetItem(i, 0, newTask);

				if (newClass != strClass)
					listCtrl->SetItem(i, 2, newClass);

				if (newStatus != strStatus) {
					listCtrl->SetItem(i, 4, newStatus);

					if (w->Hung())
						listCtrl->SetItemBackgroundColour(i, wxColour(255, 0, 0));
					else
						listCtrl->SetItemBackgroundColour(i, wxColour(255, 255, 255));
				}

				delete w;
			}
		});
	}

	void Applications::UpdateListCtrl(WindowList wl)
	{
		listCtrl->Freeze();
		listCtrl->DeleteAllItems();

		AddEntries(wl);

		listCtrl->Thaw();
	}

	void Applications::OnContextMenuEvent(wxCommandEvent& event)
	{
		int id = event.GetId();
		int ev = event.GetEventType();

		if (id == ID_LISTCTRL)
			if (LISTITEM_SELECTED() != -1)
				ShowContextMenu();

		if (id == ID_CONTEXTMENU_PROPERTIES) {
			int i = LISTITEM_SELECTED();
			if (i == -1)
				return;

			HWND h = (HWND)listCtrl->GetItemData(i);
			GString path;
			WindowManager::GetPath(h, path);
			WindowManager::OpenPropertyDialog(path);
			//wxMessageBox()
		}

		if (id == ID_CONTEXTMENU_SHOWPROCESSES) {

			int j = LISTITEM_SELECTED();
			if (j == -1)
				return;

			HWND handle = (HWND)listCtrl->GetItemData(j);
			Window *w = WindowManager::GetByHwnd(handle);
			if (!w)
				return;

			DWORD pid = w->ProcessId();

			wxListCtrl *lc = reinterpret_cast<wxListCtrl*>(FindWindowById(Processes::ID_LISTCTRL, Taskmanager::panel.processes));
			delete w;

			if (!lc)
				return;

			Taskmanager::panel.notebook->SetSelection(Taskmanager::ID_NOTEBOOK_PROC);
			lc->SetFocus();

			while (!lc->GetItemCount())
				wxMilliSleep(100);

			LOOP_LISTCTRL_DONTCARE_P(lc, {
				DWORD pid_ = (DWORD)lc->GetItemData(i);

				if (pid == pid_) {
					lc->SetItemState(i, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED);
					lc->EnsureVisible(i);
					break;
				}
			});
		}

		std::list<int> remove_ids;
		LOOP_LISTCTRL_SELECTED({
			HWND h = (HWND)listCtrl->GetItemData(i);

			if (id == ID_CONTEXTMENU_DESTROY) {
				if (!WindowManager::DestroyWindow(h)) {
					int uc = wxMessageBox(wxT("Unable to end the task!\n\nDo you want to kill the entire process?"), wxT("End task failed"), wxICON_ERROR | wxICON_QUESTION | wxYES_NO);
					if (uc == wxYES)
						if (WindowManager::TerminateProcess(h))
							remove_ids.push_back(i);
				}
				else {
					remove_ids.push_back(i);
				}
			}
			if (id == ID_CONTEXTMENU_MINIMIZE)
				WindowManager::MinimizeWindow(h);
			if (id == ID_CONTEXTMENU_MAXIMIZE)
				WindowManager::MaximizeWindow(h);
			if (id == ID_CONTEXTMENU_RESTORE)
				WindowManager::RestoreWindow(h);
			if (id == ID_CONTEXTMENU_SWITCH || id == ID_CONTEXTMENU_FOREGROUND || ev == wxEVT_LIST_ITEM_ACTIVATED) {
				if (id == ID_CONTEXTMENU_SWITCH) {
					HWND hw = GetForegroundWindow();
					WindowManager::MinimizeWindow(hw);
				}

				SetForegroundWindow(h);
				ShowWindow(h, SW_SHOWNORMAL);
				ShowWindow(h, SW_SHOW);
			}
		});

		for (auto &it : remove_ids)
			listCtrl->DeleteItem(it);

		UpdateEntries();
	}

	void Applications::OnButtonEvent(wxCommandEvent& event)
	{
		int id = event.GetId();

		if (id == ID_BUTTON_NEWTASK)
			NewTask();
	}


	void Applications::OnSearchCtrlEvent(wxCommandEvent &event)
	{
		int id = event.GetId();

		searchCtrl->ShowCancelButton(searchCtrl->GetValue().length() != 0);

		if (id == ID_SEARCHCTRL)
			UpdateListCtrl(we->List());
	}

	DWORD WINAPI Applications::TimerThread(LPVOID)
	{
		for (;;) {
			if (Taskmanager::panel.notebook->GetSelection() == 0) {
				WindowList added = we->Added();
				WindowList removed = we->Removed();

				if (we->HasUpdated(added))
					reinterpret_cast<Applications*>(Taskmanager::panel.applications)->AddEntries(added);

				if (we->HasUpdated(removed))
					reinterpret_cast<Applications*>(Taskmanager::panel.applications)->RemoveEntries(removed);

				WindowManager::FreeList(added);
				WindowManager::FreeList(removed);

				reinterpret_cast<Applications*>(Taskmanager::panel.applications)->UpdateEntries();
			}

			Sleep(TASKMGR_REFRESH_INTERVAL);
		}
	}

	TaskWindow::TaskWindow() : wxDialog(0, ID_TASK, wxT("Run"), wxDefaultPosition, wxSize(300, 115), wxCAPTION | wxCLOSE_BOX)
	{
		SetIcon(wxIcon(wxT("APPICON")));

		wxStaticText *s1 = new wxStaticText(this, wxID_ANY, wxT("Open:"));
		wxComboBox *t = new wxComboBox(this, ID_TASK_CMDLINE, wxEmptyString, wxDefaultPosition, wxSize(300, -1));
		wxButton *b1 = new wxButton(this, ID_TASK_OK, wxT("OK"));
		wxButton *b2 = new wxButton(this, ID_TASK_CANCEL, wxT("Cancel"));
		wxButton *b3 = new wxButton(this, ID_TASK_BROWSE, wxT("Browse..."));

		t->SetFocus();

		wxBoxSizer* bsm = new wxBoxSizer(wxVERTICAL);
		wxBoxSizer* bs1 = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer* bs2 = new wxBoxSizer(wxHORIZONTAL);

		bs1->Add(s1, wxSizerFlags().Border(wxRIGHT, 10));
		bs1->Add(t);

		bs2->Add(b1);
		bs2->Add(b2, wxSizerFlags().Border(wxLEFT | wxRIGHT, 5));
		bs2->Add(b3);

		bsm->Add(bs1, wxSizerFlags().Border(wxTOP | wxRIGHT | wxLEFT, 10));
		bsm->Add(bs2, wxSizerFlags().Border(wxALL, 10).Right());

		SetSizerAndFit(bsm);

		CONNECTBUTTON(this, ID_TASK_OK, TaskWindow::OnButtonEvent);
		CONNECTBUTTON(this, ID_TASK_CANCEL, TaskWindow::OnButtonEvent);
		CONNECTBUTTON(this, ID_TASK_BROWSE, TaskWindow::OnButtonEvent);
	}

	void TaskWindow::OnButtonEvent(wxCommandEvent& event)
	{
		int id = event.GetId();

		wxComboBox* c = (wxComboBox*)FindWindowById(ID_TASK_CMDLINE);
		wxButton* b = (wxButton*)FindWindowById(ID_TASK_OK);

		if (id == ID_TASK_BROWSE) {
			wxString s = wxFileSelector(wxT("Browse"), wxEmptyString, wxEmptyString, wxEmptyString,
			wxT("Programs (*.exe;*.pif;*.com;*.bat;*.cmd)|*.exe;*.pif;*.com;*.bat;*.cmd"), 0, this);

			if (s.length()) {
				c->SetValue(s);
				b->SetFocus();
			}
		}

		if (id == ID_TASK_OK) {
			wxString s = c->GetValue();
			if (s.length()) {
				//TODO: exec 'cmdline' - find a better way
				wxSystem(wxString::Format(wxT("start %s"), s));

				return;
			}

			Close();
		}
		if (id == ID_TASK_CANCEL) {
			Close();
		}
	}
}