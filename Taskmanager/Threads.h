#ifndef __THREADS__
#define __THREADS__

#include "Taskmanager.h"

namespace Threads {

	enum {
		ID_PANEL,
		ID_LISTCTRL,
		ID_SEARCHCTRL,
		ID_CHECKBOX,
		ID_CONTEXTMENU_TERMINATE,
		ID_CONTEXTMENU_SUSPEND,
		ID_CONTEXTMENU_RESUME
	};

	class Threads : public wxPanel {
	public:
		Threads(wxWindow *win, wxWindowID winid);
		~Threads();

		void ShowContextMenu();

		void Updates();
		void AddEntries(ThreadList pl);
		void RemoveEntries(ThreadList pl);
		void UpdateEntries();
		void UpdateListCtrl(ThreadList pl);

		void OnButtonEvent(wxCommandEvent &event);
		void OnCheckBoxEvent(wxCommandEvent &event);
		void OnContextMenuEvent(wxCommandEvent &event);
		void OnSearchCtrlEvent(wxCommandEvent &event);
		void OnColumnClickEvent(wxListEvent &event);

		static DWORD WINAPI TimerThread(LPVOID);
	private:
	};

}

#endif