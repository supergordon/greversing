#include "Processes.h"

namespace Processes {
	
	wxListCtrl *listCtrl;
	wxSearchCtrl *searchCtrl;
	wxImageList *imageList;

	bool all_users;
	ProcessEnum *pe;
	HANDLE handle_timer = NULL;

	Processes::Processes(wxWindow *win, wxWindowID winid) : wxPanel(win, winid)
	{
		pe = new ProcessEnum;
		all_users = false;
		imageList = new wxImageList(16, 16);

		listCtrl = new wxListCtrl(this, ID_LISTCTRL, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_SORT_ASCENDING | wxLC_VRULES);

		int c1 = listCtrl->InsertColumn(0, wxT("Process"));
		int c2 = listCtrl->InsertColumn(1, wxT("PID"), wxLIST_FORMAT_RIGHT);
		int c3 = listCtrl->InsertColumn(2, wxT("User"));
		int c4 = listCtrl->InsertColumn(3, wxT("CPU"), wxLIST_FORMAT_CENTRE);
		int c5 = listCtrl->InsertColumn(4, wxT("RAM"), wxLIST_FORMAT_RIGHT);
		int c6 = listCtrl->InsertColumn(5, wxT("Handles"), wxLIST_FORMAT_RIGHT);
		int c7 = listCtrl->InsertColumn(6, wxT("Threads"), wxLIST_FORMAT_RIGHT);
		int c8 = listCtrl->InsertColumn(7, wxT("Product Version"));
		int c9 = listCtrl->InsertColumn(8, wxT("File Description"));

		listCtrl->SetColumnWidth(c1, 150);
		listCtrl->SetColumnWidth(c2, 40);
		listCtrl->SetColumnWidth(c4, 35);
		listCtrl->SetColumnWidth(c5, 67);
		listCtrl->SetColumnWidth(c6, 55);
		listCtrl->SetColumnWidth(c7, 57);
		listCtrl->SetColumnWidth(c8, 105);
		listCtrl->SetColumnWidth(c9, 157);

		searchCtrl = new wxSearchCtrl(this, ID_SEARCHCTRL);
		searchCtrl->ShowSearchButton(false);
		searchCtrl->SetDescriptiveText(wxT("Filter..."));

		searchCtrl->Connect(ID_SEARCHCTRL, wxEVT_TEXT, wxCommandEventHandler(Processes::OnSearchCtrlEvent));
		searchCtrl->SetFocus();

		wxCheckBox *c = new wxCheckBox(this, ID_CHECKBOX, wxT("Show all users"), wxDefaultPosition, wxSize(-1, 23));

		CONNECTCHECKBOX(this, ID_CHECKBOX, Processes::OnCheckBoxEvent);

		listCtrl->Bind(wxEVT_CHAR, [](wxKeyEvent &event) {
			int key = event.GetKeyCode();

			if (key == 127) {
				wxContextMenuEvent event(wxEVT_CONTEXT_MENU, ID_CONTEXTMENU_TERMINATE);
				reinterpret_cast<Processes*>(Taskmanager::panel.processes)->OnContextMenuEvent(event);
			}
		});

		c->SetValue(all_users);

		wxBoxSizer *main = new wxBoxSizer(wxVERTICAL);
		wxBoxSizer *bar = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *bar_left = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *bar_right = new wxBoxSizer(wxHORIZONTAL);

		bar_left->Add(searchCtrl, 1, wxLEFT | wxRIGHT, 5);
		bar_right->Add(c);

		bar->Add(bar_left, 50);
		bar->Add(bar_right, 1, wxRIGHT, 5);

		main->Add(listCtrl, 1, wxGROW | wxALL, 5);
		main->Add(bar);

		SetSizerAndFit(main);

		handle_timer = ThreadManager::CreateThread(TimerThread);

		listCtrl->Connect(ID_LISTCTRL, wxEVT_CONTEXT_MENU, wxCommandEventHandler(Processes::OnContextMenuEvent));
		listCtrl->Connect(ID_LISTCTRL, wxEVT_COMMAND_LIST_COL_CLICK, wxListEventHandler(Processes::OnColumnClickEvent));

		listCtrl->Bind(wxEVT_SIZE, [](wxSizeEvent &event) {

			wxSize size = listCtrl->GetClientSize();
			float width = float(size.GetWidth()) / 100;

			listCtrl->Freeze();

			listCtrl->SetColumnWidth(0, width * 19);
			listCtrl->SetColumnWidth(1, width * 8);
			listCtrl->SetColumnWidth(2, width * 10);
			listCtrl->SetColumnWidth(3, width * 7);
			listCtrl->SetColumnWidth(4, width * 11);
			listCtrl->SetColumnWidth(5, width * 7);
			listCtrl->SetColumnWidth(6, width * 7);
			listCtrl->SetColumnWidth(7, width * 14);
			listCtrl->SetColumnWidth(8, width * 17);

			listCtrl->Thaw();
		});
	}

	Processes::~Processes()
	{
		TerminateThread(handle_timer, 0);
		CloseHandle(handle_timer);

		delete pe;
	}

	void Processes::ShowContextMenu()
	{
		wxMenu *m = new wxMenu();
		wxMenuItem *openpath = new wxMenuItem(m, ID_CONTEXTMENU_OPENPATH, wxT("Open Path"));
		wxMenuItem *restart = new wxMenuItem(m, ID_CONTEXTMENU_RESTART, wxT("Restart as admin"));
		wxMenuItem *terminate = new wxMenuItem(m, ID_CONTEXTMENU_TERMINATE, wxT("Terminate"));
		wxMenuItem *suspend = new wxMenuItem(m, ID_CONTEXTMENU_SUSPEND, wxT("Suspend"));
		wxMenuItem *resume = new wxMenuItem(m, ID_CONTEXTMENU_RESUME, wxT("Resume"));
		wxMenuItem *showhandles = new wxMenuItem(m, ID_CONTEXTMENU_SHOWHANDLES, wxT("Show handles"));
		wxMenuItem *showthreads = new wxMenuItem(m, ID_CONTEXTMENU_SHOWTHREADS, wxT("Show threads"));
		wxMenuItem *hide = new wxMenuItem(m, ID_CONTEXTMENU_HIDE, wxT("Hide"));
		wxMenuItem *protect = new wxMenuItem(m, ID_CONTEXTMENU_PROTECT, wxT("Protect"));
		wxMenuItem *endtask = new wxMenuItem(m, ID_CONTEXTMENU_DUMP, wxT("Dump"));
		wxMenuItem *prop = new wxMenuItem(m, ID_CONTEXTMENU_PROPERTIES, wxT("Properties"));
		//setadmin rights

		m->Append(openpath);
		m->Append(restart);
		m->AppendSeparator();
		m->Append(terminate);
		m->Append(suspend);
		m->Append(resume);
		m->AppendSeparator();
		m->Append(showhandles);
		m->Append(showthreads);
		m->AppendSeparator();
		m->Append(hide);
		m->Append(protect);
		m->AppendSeparator();
		m->Append(prop);

		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_OPENPATH, Processes::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_RESTART, Processes::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_TERMINATE, Processes::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_SUSPEND, Processes::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_RESUME, Processes::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_SHOWHANDLES, Processes::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_SHOWTHREADS, Processes::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_HIDE, Processes::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_PROTECT, Processes::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_DUMP, Processes::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_PROPERTIES, Processes::OnContextMenuEvent);

		PopupMenu(m);
	}

	void Processes::Updates()
	{
		pe->Clear();

		ProcessList updated = pe->Updated();

		if (!pe->HasUpdated(updated))
			return;

		ProcessManager::FreeList(updated);

		UpdateListCtrl(pe->List());
	}

	void Processes::AddEntries(const ProcessList &pl)
	{
		Process *p = ProcessManager::GetByProcessId(ProcessManager::GetCurrentProcessId(), &pe->List(), false);
		if (!p)
			return;

		wxString filter = searchCtrl->GetValue();

		for (auto &it : pl) {
			if (!all_users) {
				if (it->User() != p->User())
					continue;
			}

			wxString name = it->Name();
			wxString user = it->User();
			wxString desc = it->Description(L"FileDescription", 1);
			wxString ver = it->Description(L"ProductVersion", 2);
			wxString pid = wxString::Format(wxT("%d"), it->ProcessId());

			if (user.length() == 0)
				user.Append(wxT("UNKNOWN"));

			if (desc.length() == 0)
				desc = it->Name();

			if (ver.length() == 0)
				ver = it->Description(L"ProductVersion");

			if (ver.length() == 0)
				ver.Append(wxT("N/A"));

			if (it->Wow64())
				name.Append(L"*");

			if (it->Admin())
				name.Append(L"+");

			wxString strname = name;
			wxString struser = user;
			wxString strdesc = desc;
			wxString strver = ver;
			wxString strpid = pid;

			filter.MakeLower();
			strname.MakeLower();
			struser.MakeLower();
			strdesc.MakeLower();
			strver.MakeLower();

			if (filter.length()) {
				if (!strname.Contains(filter))
					if (!struser.Contains(filter))
						if (!strdesc.Contains(filter))
							if (!strver.Contains(filter))
								continue;
			}

			int image_index = 0;
			HICON handle_icon = ProcessManager::GetAppIcon(it->ProcessId());
			if (handle_icon) {
				wxIcon icon;
				icon.SetHICON(handle_icon);
				image_index = imageList->Add(icon);
				DestroyIcon(handle_icon);
			}
			else {
				image_index = imageList->Add(wxIcon(wxT("APPICON")));
			}

			wxString used_memory;
			float memory = static_cast<float>(it->UsedMemory());

			wxString num_threads = wxString::Format(wxT("%d"), it->NumThreads());

			wxString num_handles = wxString::Format(wxT("%d"), it->NumHandles());

			wxString process_id = wxString::Format(wxT("%d"), it->ProcessId());

			wxString used_cpu = wxString::Format(wxT("%.2f"), it->UsedCPU());

			if (memory <= 1024)
				used_memory = wxString::Format(wxT("%d B"), memory);
			else if (memory / 1024 <= 1024)
				used_memory = wxString::Format(wxT("%.0f KB"), memory / 1024);
			else if (memory / 1024 / 1024 <= 1024)
				used_memory = wxString::Format(wxT("%.2f MB"), memory / 1024 / 1024);
			else
				used_memory = wxString::Format(wxT("%.1f GB"), memory / 1024 / 1024 / 1024);

			int i = listCtrl->InsertItem(0, name, image_index);
			int i1 = listCtrl->SetItem(i, 1, pid);
			int i2 = listCtrl->SetItem(i, 2, user);
			int i3 = listCtrl->SetItem(i, 3, used_cpu);
			int i4 = listCtrl->SetItem(i, 4, used_memory);
			int i5 = listCtrl->SetItem(i, 5, num_handles);
			int i6 = listCtrl->SetItem(i, 6, num_threads);
			int i7 = listCtrl->SetItem(i, 7, ver);
			int i8 = listCtrl->SetItem(i, 8, desc);

			listCtrl->SetItemData(i, it->ProcessId());

			//if (first) {
			//	listCtrl->SetItemTextColour(i, wxColour("red"));
			//	//wxLogWarning("1: %d", i);
			//	CloseHandle(CreateThread(0, 0, [](LPVOID j) -> DWORD {
			//		Sleep(1000);
			//		listCtrl->SetItemTextColour(long(j), wxColour("black"));
			//		//wxLogWarning("2: %d", long(j));
			//		return 1;

			//	}, (LPVOID)i, 0, 0));
			//}
		}

		listCtrl->SetImageList(imageList, wxIMAGE_LIST_SMALL);
	}

	void Processes::RemoveEntries(const ProcessList &pl)
	{
		for (auto &it : pl) {
			LOOP_LISTCTRL_DONTCARE({
				DWORD pid = (DWORD)listCtrl->GetItemData(i);

				if (pid == it->ProcessId())
					listCtrl->DeleteItem(i);
			});
		}
	}

	void Processes::UpdateEntries()
	{
		ProcessList list = pe->List();

		LOOP_LISTCTRL_DONTCARE({
			wxRect rect_listctrl = listCtrl->GetRect();
			wxPoint pt_item;
			listCtrl->GetItemPosition(i, pt_item);

			if (pt_item.y > rect_listctrl.height)
				continue;

			wxString strPID = listCtrl->GetItemText(i, 1);
			wxString strCPU = listCtrl->GetItemText(i, 3);
			wxString strRAM = listCtrl->GetItemText(i, 4);
			wxString strHandles = listCtrl->GetItemText(i, 5);
			wxString strThreads = listCtrl->GetItemText(i, 6);

			DWORD pid;
			strPID.ToULong(&pid);

			DWORD handles;
			strHandles.ToULong(&handles);

			DWORD threads;
			strThreads.ToULong(&threads);

			DWORD PID = listCtrl->GetItemData(i);

			if (!ProcessManager::IsValid(PID)) {
				listCtrl->DeleteItem(i);
				return;
			}

			Process *p = ProcessManager::GetByProcessId(PID, &list, false);
			if (p) {
				float new_cpu = p->UsedCPU();
				DWORD new_handles = p->NumHandles();
				DWORD new_threads = p->NumThreads();

				wxString newCpu = wxString::Format(L"%.2f", new_cpu);
				wxString newHandles = wxString::Format(L"%d", new_handles);
				wxString newThreads = wxString::Format(L"%d", new_threads);

				wxString newMemory;
				float memory = static_cast<float>(p->UsedMemory());
				if (memory <= 1024)
					newMemory = wxString::Format(wxT("%d B"), memory);
				else if (memory / 1024 <= 1024)
					newMemory = wxString::Format(wxT("%.0f KB"), memory / 1024);
				else if (memory / 1024 / 1024 <= 1024)
					newMemory = wxString::Format(wxT("%.2f MB"), memory / 1024 / 1024);
				else
					newMemory = wxString::Format(wxT("%.1f GB"), memory / 1024 / 1024 / 1024);

				if (newCpu != strCPU) {
					listCtrl->SetItem(i, 3, newCpu);
					wxColor color;

					if (new_cpu > 90)
						color = wxColour(139, 0, 0); //darkred
					else if (new_cpu > 50)
						color = wxColour(255, 0, 0);
					else if (new_cpu > 20)
						color = wxColour(255, 110, 0); //orangered
					else if (new_cpu > 10)
						color = wxColour(0, 139, 0);
					else
						color = wxColour(0, 0, 0);

					if(color != listCtrl->GetItemTextColour(i))
						listCtrl->SetItemTextColour(i, color);
				}

				if (newMemory != strRAM)
					listCtrl->SetItem(i, 4, newMemory);

				if (handles != new_handles)
					listCtrl->SetItem(i, 5, newHandles);

				if (threads != new_threads)
					listCtrl->SetItem(i, 6, newThreads);

				//delete p;
			}
		});
	}

	void Processes::UpdateListCtrl(const ProcessList &pl)
	{
		listCtrl->Freeze();
		listCtrl->DeleteAllItems();

		AddEntries(pl);

		listCtrl->Thaw();
	}

	void Processes::OnButtonEvent(wxCommandEvent &event)
	{
		int id = event.GetId();
	}

	void Processes::OnCheckBoxEvent(wxCommandEvent &event)
	{
		int id = event.GetId();

		if (id == ID_CHECKBOX) {
			all_users = event.IsChecked();

			UpdateListCtrl(pe->List());
		}
	}

	void Processes::OnContextMenuEvent(wxCommandEvent &event)
	{
		int id = event.GetId();

		if (id == ID_LISTCTRL)
			if (LISTITEM_SELECTED() != -1)
				ShowContextMenu();

		if (id == ID_CONTEXTMENU_DUMP)
			wxMessageBox(L"dump");

		if (id == ID_CONTEXTMENU_PROPERTIES) {
			int i = LISTITEM_SELECTED();
			if (i == -1)
				return;

			DWORD pid = (DWORD)listCtrl->GetItemData(i);
			GString path;
			ProcessManager::GetPath(pid, path);
			WindowManager::OpenPropertyDialog(path);
		}

		std::list<int> remove_ids;
	
		LOOP_LISTCTRL_SELECTED({
			DWORD pid = (DWORD)listCtrl->GetItemData(i);

			if (id == ID_CONTEXTMENU_TERMINATE) {

				GString name;
				ProcessManager::GetName(pid, name);

				if (name == _G("csrss.exe")) {
					int ret = wxMessageBox(wxString::Format(wxT("Terminating %s will result in a system failure.\n\nAre you sure you want to continue?"), name), wxT("Warning"), wxICON_STOP | wxYES_NO);
					if (ret == wxNO)
						return;
				}

				if (!ProcessManager::Terminate(pid)) {
					Process *p = ProcessManager::GetByProcessId(pid);

					int ret = wxMessageBox(wxString::Format(wxT("Could not terminate process. %ls may be protected.\n\nDo you want to terminate using a kernel mode driver?"), p->Name().c_str()), wxT("Warning"), wxICON_EXCLAMATION | wxYES_NO);
					delete p;

					if (ret == wxYES) {
						DriverInterface di;

						bool registered = di.Register();
						bool started = di.Start();

						if (di.TerminateProcess(pid))
							remove_ids.push_back(i);

						if (started)
							di.Stop();

						if (registered)
							di.Unregister();
					}
				}
				else {
					remove_ids.push_back(i);
				}
			}

			if (id == ID_CONTEXTMENU_HIDE) {
				Process *p = ProcessManager::GetByProcessId(pid);
				int ret = wxMessageBox(wxString::Format(wxT("Do you really want to hide %ls using a kernel mode driver?\n\nTHIS WILL RESULT IN BSOD WITH PATCHGUARD ENABLED!"), p->Name().c_str()), wxT("Warning"), wxICON_WARNING | wxYES_NO);
				delete p;

				if (ret == wxYES) {
					DriverInterface di;

					bool registered = di.Register();
					bool started = di.Start();

					di.HideProcess(pid);

					if (started)
						di.Stop();

					if (registered)
						di.Unregister();

				}
			}

			if (id == ID_CONTEXTMENU_PROTECT) {
				Process *p = ProcessManager::GetByProcessId(pid);
				int ret = wxMessageBox(wxString::Format(wxT("Do you really want to protect %ls using a kernel mode driver?\n\nThis will make the process not accessable."), p->Name().c_str()), wxT("Warning"), wxICON_QUESTION | wxYES_NO);
				delete p;

				if (ret == wxYES) {
					DriverInterface di;

					bool registered = di.Register();
					bool started = di.Start();

					if (!di.ProtectProcess(pid))
						wxMessageBox("Could not protect process.\n\nBe sure the kernel driver can be loaded!", "Error", wxICON_ERROR);

					if (started)
						di.Stop();

					if (registered)
						di.Unregister();
				}
			}

			if (id == ID_CONTEXTMENU_OPENPATH) {
				GString path;
				if (ProcessManager::GetPath(pid, path)) 
					WindowManager::OpenInExplorer(path);
				else
					wxMessageBox("Could not open path in explorer.\n\nSeems like we do not have enough rights.", wxT("Error"), wxICON_EXCLAMATION);
			}

			if(id == ID_CONTEXTMENU_RESTART) {
				if (!ProcessManager::RestartAsAdmin(pid))
					wxMessageBox("Could not restart application as admininstrator.\n\nApplication may already be running as administrator or correct privileges are missing to restart as admin.", "Error", wxICON_ERROR);
			}

			if (id == ID_CONTEXTMENU_SUSPEND)
				if (!ProcessManager::Suspend(pid))
					wxMessageBox(wxT("Could not suspend process.\n\nProcess may be protected or was already suspended."), wxT("Error"), wxICON_EXCLAMATION);

			if (id == ID_CONTEXTMENU_RESUME)
				if (!ProcessManager::Resume(pid))
					wxMessageBox(wxT("Could not resume process.\n\nProcess may be protected or was already resumed."), wxT("Error"), wxICON_EXCLAMATION);

			if (id == ID_CONTEXTMENU_SHOWTHREADS) {
				Taskmanager::panel.notebook->SetSelection(Taskmanager::ID_NOTEBOOK_THREAD);

				wxSearchCtrl *sc = reinterpret_cast<wxSearchCtrl*>(FindWindowById(Threads::ID_SEARCHCTRL, Taskmanager::panel.threads));
				Process *p = ProcessManager::GetByProcessId(pid);

				sc->SetValue(p->Name());

				delete p;
				return;
			}

			if (id == ID_CONTEXTMENU_SHOWHANDLES) {
				Taskmanager::panel.notebook->SetSelection(Taskmanager::ID_NOTEBOOK_HANDLE);

				wxSearchCtrl *sc = reinterpret_cast<wxSearchCtrl*>(FindWindowById(Handles::ID_SEARCHCTRL, Taskmanager::panel.handles));
				Process *p = ProcessManager::GetByProcessId(pid);

				sc->SetValue(p->Name());

				delete p;
				return;
			}
		});

		for (auto &it : remove_ids) {
			listCtrl->DeleteItem(it);
		}
		remove_ids.clear();

		UpdateEntries();
	}

	void Processes::OnSearchCtrlEvent(wxCommandEvent &event)
	{
		int id = event.GetId();

		searchCtrl->ShowCancelButton(searchCtrl->GetValue().length() != 0);

		if (id == ID_SEARCHCTRL)
			UpdateListCtrl(pe->List());

	}

	void Processes::OnColumnClickEvent(wxListEvent &event)
	{
		listCtrl->SortItems(
		[](wxIntPtr, wxIntPtr, wxIntPtr) { return 1; }, 
		(wxIntPtr)listCtrl);
	}

	DWORD WINAPI Processes::TimerThread(LPVOID)
	{
		for (;;) {
			if (Taskmanager::panel.notebook->GetSelection() == 1) {
				ProcessList added = pe->Added();
				ProcessList removed = pe->Removed();

				if (pe->HasUpdated(added))
					reinterpret_cast<Processes*>(Taskmanager::panel.processes)->AddEntries(added);

				if (pe->HasUpdated(removed))
					reinterpret_cast<Processes*>(Taskmanager::panel.processes)->RemoveEntries(removed);

				ProcessManager::FreeList(added);
				ProcessManager::FreeList(removed);

				reinterpret_cast<Processes*>(Taskmanager::panel.processes)->UpdateEntries();
			}

			Sleep(TASKMGR_REFRESH_INTERVAL);
		}
	}

}