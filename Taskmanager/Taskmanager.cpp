#include "Taskmanager.h"

namespace Taskmanager {

	Panel panel;
	int active_page = ID_NOTEBOOK_PROC;

	wxFont font(9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, _G("Calibri"));

	Taskmanager::Taskmanager() : wxFrame(NULL, ID_FRAME, wxT(W_NAME_TASKMGR), wxDefaultPosition, wxSize(695, 475))
	{
		panel.main = this;
		SetFont(font);
		wxString name = this->GetTitle();

#if defined (_M_X64)
		name.Append(wxT(" (x64 version)"));
#else
		name.Append(wxT(" (x86 version)"));
#endif

		this->SetTitle(name);

		wxInitAllImageHandlers();
		SetIcon(wxIcon(wxT("APPICON")));

		//Statusbar
		wxStatusBar* statusBar = CreateStatusBar(1, 0);
		wxStaticText* statusBarText = new wxStaticText(statusBar, ID_STATUSBAR, wxString::Format(L"%s (%s)", APP_NAME, W_NAME_TASKMGR), wxPoint(5, 5), wxSize(-1, -1));
	
		wxNotebook* nb = new wxNotebook(this, ID_NOTEBOOK, wxDefaultPosition, wxDefaultSize, wxNB_TOP /*| wxNB_FIXEDWIDTH| wxNB_NOPAGETHEME */);

		panel.notebook = nb;

		panel.applications = new Applications::Applications(nb, ID_PANEL_APP);//new wxPanel(nb, D_PANEL_APP);
		panel.processes = new Processes::Processes(nb, ID_PANEL_PROC);//new wxPanel(nb, D_PANEL_PROC);
		panel.threads = new Threads::Threads(nb, ID_PANEL_THREAD);//new wxPanel(nb, D_PANEL_THREAD);
		panel.handles = new Handles::Handles(nb, ID_PANEL_HANDLES);
		panel.services = new Services::Services(nb, ID_PANEL_SERV);//new wxPanel(nb, D_PANEL_SERV);
		panel.performance = new wxPanel(nb, ID_PANEL_PERF);
		panel.network = new wxPanel(nb, ID_PANEL_NET);
		panel.user = new wxPanel(nb, ID_PANEL_USER);
		panel.options = new Options::Options(nb, ID_PANEL_OPT);//new wxPanel(nb, D_PANEL_OPT);

		nb->AddPage(panel.applications, wxT("Applications"));
		nb->AddPage(panel.processes, wxT("Processes"));
		nb->AddPage(panel.threads, wxT("Threads"));
		nb->AddPage(panel.handles, wxT("Handles"));
		nb->AddPage(panel.services, wxT("Services"));
		nb->AddPage(panel.performance, wxT("Performance"));
		nb->AddPage(panel.network, wxT("Network"));
		nb->AddPage(panel.user, wxT("User"));
		nb->AddPage(panel.options, "Options");

		wxBoxSizer* bs = new wxBoxSizer(wxVERTICAL);
		bs->Add(nb, 1, wxGROW | wxALL, 0);
		SetSizer(bs);

		nb->Connect(wxEVT_NOTEBOOK_PAGE_CHANGED, wxBookCtrlEventHandler(Taskmanager::OnNotebookSelection));

		wxBookCtrlEvent ev(wxEVT_NULL, wxID_ANY, active_page);
		OnNotebookSelection(ev);

		nb->SetSelection(active_page);
		SetMinSize(wxSize(600, 350));
	}

	Taskmanager::~Taskmanager()
	{

	}

	void Taskmanager::OnNotebookSelection(wxBookCtrlEvent &event)
	{
		int id = event.GetId();
		int sel = event.GetSelection();

		if (sel == ID_NOTEBOOK_APP)
			reinterpret_cast<Applications::Applications*>(panel.applications)->Updates();

		if (sel == ID_NOTEBOOK_PROC)
			reinterpret_cast<Processes::Processes*>(panel.processes)->Updates();

		if (sel == ID_NOTEBOOK_THREAD)
			reinterpret_cast<Threads::Threads*>(panel.threads)->Updates();

		if (sel == ID_NOTEBOOK_HANDLE)
			reinterpret_cast<Handles::Handles*>(panel.handles)->Updates();

		if (sel == ID_NOTEBOOK_SERV)
			reinterpret_cast<Services::Services*>(panel.services)->Updates();
	}
}