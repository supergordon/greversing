#ifndef __HANDLES__
#define __HANDLES__

#include "Taskmanager.h"

namespace Handles {

	enum {
		ID_PANEL,
		ID_LISTCTRL,
		ID_SEARCHCTRL,
		ID_CHECKBOX_FILE,
		ID_CHECKBOX_PROCESS,
		ID_CHECKBOX_THREAD,
		ID_CHECKBOX_KEY,
		ID_CHECKBOX_OTHER,
	};

	typedef struct _SYSTEM_HANDLE {
		ULONG ProcessId;
		BYTE ObjectTypeNumber;
		BYTE Flags;
		USHORT Handle;
		PVOID64 Object;
		ACCESS_MASK GrantedAccess;
	} SYSTEM_HANDLE, *PSYSTEM_HANDLE;

	typedef struct _SYSTEM_HANDLE_INFORMATION {
		ULONG HandleCount;
		SYSTEM_HANDLE Handles[1];
	} SYSTEM_HANDLE_INFORMATION, *PSYSTEM_HANDLE_INFORMATION;

	typedef struct _DEVICE_CONTROL_DATA {
		PVOID64 address;
		LONG pid;
		LONG type;
		USHORT handle;
		wchar_t pad[260];
	} DEVICE_CONTROL_DATA;

	typedef struct _FILE_INFO {
		wchar_t filename[260];
		ULONG uType;
	} FILE_INFO;

#define IOCTL_GDRIVER_OPENFILES CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800, METHOD_BUFFERED, FILE_READ_DATA | FILE_WRITE_DATA)

	class Handles : public wxPanel {
	public:
		Handles(wxWindow *win, wxWindowID winid);
		~Handles();

		void ShowContextMenu();

		void Updates();
		void AddEntries(HandleList hl);
		void RemoveEntries(HandleList hl);
		void UpdateEntries();
		void UpdateListCtrl(HandleList hl);

		void OnButtonEvent(wxCommandEvent &event);
		void OnCheckBoxEvent(wxCommandEvent &event);
		void OnContextMenuEvent(wxCommandEvent &event);
		void OnSearchCtrlEvent(wxCommandEvent &event);

		static DWORD WINAPI TimerThread(LPVOID);
	private:
	};

}

#endif