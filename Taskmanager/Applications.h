#ifndef __APPLICATIONS__
#define __APPLICATIONS__

#include "Taskmanager.h"

namespace Applications {

	enum {
		ID_LISTCTRL,
		ID_SEARCHCTRL,
		ID_BUTTON_NEWTASK,
		ID_CONTEXTMENU_NEWTASK,
		ID_CONTEXTMENU_SWITCH,
		ID_CONTEXTMENU_FOREGROUND,
		ID_CONTEXTMENU_MINIMIZE,
		ID_CONTEXTMENU_MAXIMIZE,
		ID_CONTEXTMENU_RESTORE,
		ID_CONTEXTMENU_DESTROY,
		ID_CONTEXTMENU_SHOWPROCESSES,
		ID_CONTEXTMENU_PROPERTIES,
		ID_CONTEXTMENU_OPENPATH,

		ID_TASK,
		ID_TASK_CMDLINE,
		ID_TASK_OK,
		ID_TASK_CANCEL,
		ID_TASK_BROWSE
	};

	class Applications : public wxPanel {
	public:
		Applications(wxWindow *win, wxWindowID winid);
		~Applications();

		void ShowContextMenu();
		void NewTask(wxString cmdline = "");

		void Updates();
		void AddEntries(WindowList wl);
		void RemoveEntries(WindowList wl);
		void UpdateEntries();
		void UpdateListCtrl(WindowList wl);

		void OnContextMenuEvent(wxCommandEvent &event);
		void OnButtonEvent(wxCommandEvent &event);

		void OnSearchCtrlEvent(wxCommandEvent &event);

		static DWORD WINAPI TimerThread(LPVOID);
	private:
	};

	class TaskWindow : public wxDialog {
	public:
		TaskWindow();

		void OnButtonEvent(wxCommandEvent &event);
	};
}

#endif