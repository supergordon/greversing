#include "Services.h"

namespace Services {

	wxListCtrl *listCtrl;
	wxSearchCtrl *searchCtrl;
	wxImageList *imageList;

	bool filter_kernel;
	bool filter_file;
	bool filter_own;
	bool filter_share;
	bool filter_interactive;
	ServiceEnum *se;
	HANDLE handle_timer = NULL;

	Services::Services(wxWindow *win, wxWindowID winid) : wxPanel(win, winid)
	{
		se = new ServiceEnum;
		filter_kernel = true;
		filter_file = true;
		filter_own = false;
		filter_share = false;
		filter_interactive = false;

		imageList = new wxImageList(16, 16);

		listCtrl = new wxListCtrl(this, ID_LISTCTRL, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_SORT_ASCENDING | wxLC_VRULES);

		int c1 = listCtrl->InsertColumn(0, wxT("Service Name"));
		int c2 = listCtrl->InsertColumn(1, wxT("Display Name"));
		int c3 = listCtrl->InsertColumn(2, wxT("PID"), wxLIST_FORMAT_RIGHT);
		int c4 = listCtrl->InsertColumn(3, wxT("Type"));
		int c5 = listCtrl->InsertColumn(4, wxT("State"));
		int c6 = listCtrl->InsertColumn(5, wxT("Start Type"));

		listCtrl->SetColumnWidth(c1, 150);
		listCtrl->SetColumnWidth(c2, 250);
		listCtrl->SetColumnWidth(c3, 65);
		listCtrl->SetColumnWidth(c4, 100);
		listCtrl->SetColumnWidth(c5, 70);
		listCtrl->SetColumnWidth(c6, 95);

		searchCtrl = new wxSearchCtrl(this, ID_SEARCHCTRL);
		searchCtrl->ShowSearchButton(false);
		searchCtrl->SetDescriptiveText(wxT("Filter..."));

		searchCtrl->Connect(ID_SEARCHCTRL, wxEVT_TEXT, wxCommandEventHandler(Services::OnSearchCtrlEvent));

		wxCheckBox *cb1 = new wxCheckBox(this, ID_CHECKBOX_KERNEL, wxT("K"), wxDefaultPosition, wxSize(-1, 23));
		wxCheckBox *cb2 = new wxCheckBox(this, ID_CHECKBOX_FILE, wxT("F"), wxDefaultPosition, wxSize(-1, 23));
		wxCheckBox *cb3 = new wxCheckBox(this, ID_CHECKBOX_OWN, wxT("O"), wxDefaultPosition, wxSize(-1, 23));
		wxCheckBox *cb4 = new wxCheckBox(this, ID_CHECKBOX_SHARE, wxT("S"), wxDefaultPosition, wxSize(-1, 23));

		cb1->SetToolTip("Kernel driver service");
		cb2->SetToolTip("File system driver service");
		cb3->SetToolTip("Services that run in their own processes");
		cb4->SetToolTip("Services that share a process with one or more other services.");

		cb1->SetValue(!filter_kernel);
		cb2->SetValue(!filter_file);
		cb3->SetValue(!filter_own);
		cb4->SetValue(!filter_share);

		CONNECTCHECKBOX(this, ID_CHECKBOX_KERNEL, Services::OnCheckBoxEvent);
		CONNECTCHECKBOX(this, ID_CHECKBOX_FILE, Services::OnCheckBoxEvent);
		CONNECTCHECKBOX(this, ID_CHECKBOX_OWN, Services::OnCheckBoxEvent);
		CONNECTCHECKBOX(this, ID_CHECKBOX_SHARE, Services::OnCheckBoxEvent);

		wxBoxSizer *main = new wxBoxSizer(wxVERTICAL);
		wxBoxSizer *bar = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *bar_left = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *bar_right = new wxBoxSizer(wxHORIZONTAL);

		bar_left->Add(searchCtrl, 1, wxLEFT | wxRIGHT, 5);
		bar_right->Add(cb1);
		bar_right->Add(cb2);
		bar_right->Add(cb3);
		bar_right->Add(cb4);

		bar->Add(bar_left, 50);
		bar->Add(bar_right, 1, wxRIGHT, 5);

		main->Add(listCtrl, 1, wxGROW | wxALL, 5);
		main->Add(bar);

		SetSizerAndFit(main);

		handle_timer = ThreadManager::CreateThread(TimerThread);

		listCtrl->Connect(ID_LISTCTRL, wxEVT_CONTEXT_MENU, wxCommandEventHandler(Services::OnContextMenuEvent));

		listCtrl->Bind(wxEVT_SIZE, [](wxSizeEvent &event) {

			wxSize size = listCtrl->GetClientSize();
			float width = float(size.GetWidth()) / 100;

			listCtrl->Freeze();

			listCtrl->SetColumnWidth(0, width * 24);
			listCtrl->SetColumnWidth(1, width * 27);
			listCtrl->SetColumnWidth(2, width * 8);
			listCtrl->SetColumnWidth(3, width * 16);
			listCtrl->SetColumnWidth(4, width * 10);
			listCtrl->SetColumnWidth(5, width * 15);

			listCtrl->Thaw();
		});
	}

	Services::~Services()
	{
		TerminateThread(handle_timer, 0);
		CloseHandle(handle_timer);

		delete se;
	}

	void Services::ShowContextMenu()
	{
		wxMenu *m = new wxMenu();
		wxMenuItem *start = new wxMenuItem(m, ID_CONTEXTMENU_START, wxT("Start"));
		wxMenuItem *stop = new wxMenuItem(m, ID_CONTEXTMENU_STOP, wxT("Stop"));
		wxMenuItem *unregister = new wxMenuItem(m, ID_CONTEXTMENU_UNREGISTER, wxT("Unregister"));
		wxMenuItem *openpath = new wxMenuItem(m, ID_CONTEXTMENU_OPENPATH, wxT("Open Path"));

		m->Append(start);
		m->Append(stop);
		m->Append(unregister);
		m->AppendSeparator();
		m->Append(openpath);

		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_START, Services::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_STOP, Services::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_UNREGISTER, Services::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_OPENPATH, Services::OnContextMenuEvent);

		PopupMenu(m);
	}

	void Services::Updates()
	{
		se->Clear();

		ServiceList updated = se->Updated();

		if (!se->HasUpdated(updated))
			return;

		ServiceManager::FreeList(updated);

		UpdateListCtrl(se->List());
	}

	void Services::AddEntries(const ServiceList &sl)
	{
		wxString filter = searchCtrl->GetValue();

		for (auto &it : sl) {

			if (filter_kernel && (it->Type() & SERVICE_KERNEL_DRIVER))
				continue;

			if (filter_file && (it->Type() & SERVICE_FILE_SYSTEM_DRIVER))
				continue;

			if (filter_own && (it->Type() & SERVICE_WIN32_OWN_PROCESS))
				continue;

			if (filter_share && (it->Type() & SERVICE_WIN32_SHARE_PROCESS))
				continue;

			wxString name = it->Name();
			wxString display = it->DisplayName();
			wxString pid = it->ProcessId() ? wxString::Format("%d", it->ProcessId()) : "";
			wxString type = it->TypeName();
			wxString state = it->CurrentStateName();
			wxString starttype = it->StartTypeName();


			wxString strName = name;
			wxString strDisplay = display;
			wxString strType = type;
			wxString strState = state;

			filter.MakeLower();
			strName.MakeLower();
			strDisplay.MakeLower();
			strType.MakeLower();
			strState.MakeLower();

			if (filter.length())
				if (!strName.Contains(filter))
					if (!strDisplay.Contains(filter))
						if (!strType.Contains(filter))
							if (!strState.Contains(filter))
								continue;

			int image_index = 0;
			HICON handle_icon = ProcessManager::GetAppIcon((*it).Path());

			if (handle_icon) {
				wxIcon icon;
				icon.SetHICON(handle_icon);
				image_index = imageList->Add(icon);
				DestroyIcon(handle_icon);
			}
			else {
				image_index = imageList->Add(wxIcon(wxT("APPICON")));
			}

			int i = listCtrl->InsertItem(0, name, image_index);
			int i1 = listCtrl->SetItem(i, 1, display);
			int i2 = listCtrl->SetItem(i, 2, pid);
			int i3 = listCtrl->SetItem(i, 3, type);
			int i4 = listCtrl->SetItem(i, 4, state);
			int i5 = listCtrl->SetItem(i, 5, starttype);

			if (it->CurrentState() == SERVICE_RUNNING)
				listCtrl->SetItemBackgroundColour(i, wxColour(200, 200, 200));
			
		}

		listCtrl->SetImageList(imageList, wxIMAGE_LIST_SMALL);
	}

	void Services::RemoveEntries(const ServiceList &sl)
	{
		for (auto &it : sl) {
			LOOP_LISTCTRL_DONTCARE({
				wxString s1 = listCtrl->GetItemText(i, 0);
				if (s1.length() == 0)
					continue;

				if (s1 == it->Name())
					listCtrl->DeleteItem(i);
			});
		}
	}

	void Services::UpdateEntries()
	{
		ServiceList list = se->List();

		LOOP_LISTCTRL_DONTCARE({
			wxRect rect_listctrl = listCtrl->GetRect();
			wxPoint pt_item;
			listCtrl->GetItemPosition(i, pt_item);

			if (pt_item.y > rect_listctrl.height)
				continue;

			wxString name = listCtrl->GetItemText(i, 0);
			wxString state = listCtrl->GetItemText(i, 4);

			Service *s = ServiceManager::GetByName(GString(name.wchar_str()), &list, false);
			if (s) {
				wxString newName = s->Name();
				wxString newState;

				if (s->CurrentState() == SERVICE_STOPPED)
					newState = "Stopped";

				if (s->CurrentState() == SERVICE_START_PENDING)
					newState = "Starting";

				if (s->CurrentState() == SERVICE_STOP_PENDING)
					newState = "Stopping";

				if (s->CurrentState() == SERVICE_RUNNING)
					newState = "Running";

				if (s->CurrentState() == SERVICE_CONTINUE_PENDING)
					newState = "Continuing";

				if (s->CurrentState() == SERVICE_PAUSE_PENDING)
					newState = "Pausing";

				if (s->CurrentState() == SERVICE_PAUSED)
					newState = "Paused";

				if (newState != state)
					listCtrl->SetItem(i, 4, newState);

				//delete s;
			}
		});
	}

	void Services::UpdateListCtrl(const ServiceList &sl)
	{
		//listCtrl->Freeze();
		listCtrl->DeleteAllItems();

		AddEntries(sl);

		//listCtrl->Thaw();
	}

	void Services::OnButtonEvent(wxCommandEvent &event)
	{

	}

	void Services::OnCheckBoxEvent(wxCommandEvent &event)
	{
		int id = event.GetId();

		if (id == ID_CHECKBOX_KERNEL)
			filter_kernel = !event.IsChecked();

		if (id == ID_CHECKBOX_FILE)
			filter_file = !event.IsChecked();

		if (id == ID_CHECKBOX_OWN)
			filter_own = !event.IsChecked();

		if (id == ID_CHECKBOX_SHARE)
			filter_share = !event.IsChecked();

		UpdateListCtrl(se->List());
	}

	void Services::OnContextMenuEvent(wxCommandEvent &event)
	{
		int id = event.GetId();

		if (id == ID_LISTCTRL)
			if (LISTITEM_SELECTED() != -1)
				ShowContextMenu();

		LOOP_LISTCTRL_SELECTED({
			wxString s1 = listCtrl->GetItemText(i, 0);
			if (s1.length() == 0)
				return;

			Service *s = ServiceManager::GetByName(GString(s1.wchar_str()));
			if (s) {
				if (id == ID_CONTEXTMENU_START)
					if (!s->Start())
						wxMessageBox("Could not start service.\n\nAn error occured.");

				if (id == ID_CONTEXTMENU_STOP)
					if (!s->Stop())
						wxMessageBox("Could not stop service.\n\nAn error occured.");

				if (id == ID_CONTEXTMENU_UNREGISTER)
					if (!s->Unregister())
						wxMessageBox("Could not unregister service.\n\nAn error occured.");

				if (id == ID_CONTEXTMENU_OPENPATH) {
					WindowManager::OpenInExplorer(s->Path());
				}

				delete s;
			}
		});
	}

	void Services::OnSearchCtrlEvent(wxCommandEvent &event)
	{
		int id = event.GetId();

		searchCtrl->ShowCancelButton(searchCtrl->GetValue().length() != 0);

		if (id == ID_SEARCHCTRL)
			UpdateListCtrl(se->List());
	}

	DWORD WINAPI Services::TimerThread(LPVOID)
	{
		for (;;) {
			if (Taskmanager::panel.notebook->GetSelection() == 4) {
				ServiceList added = se->Added();
				ServiceList removed = se->Removed();

				if (se->HasUpdated(added))
					reinterpret_cast<Services*>(Taskmanager::panel.services)->AddEntries(added);

				if (se->HasUpdated(removed))
					reinterpret_cast<Services*>(Taskmanager::panel.services)->RemoveEntries(removed);

				ServiceManager::FreeList(added);
				ServiceManager::FreeList(removed);

				reinterpret_cast<Services*>(Taskmanager::panel.services)->UpdateEntries();
			}

			Sleep(TASKMGR_REFRESH_INTERVAL);
		}
	}

}