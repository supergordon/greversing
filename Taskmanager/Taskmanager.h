#ifndef __TASKMANAGER__
#define __TASKMANAGER__

#pragma warning (disable: 4996)

#include "..\vars.h"
#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/srchctrl.h>
#include <wx/event.h>
#include <wx/notebook.h>

#include "F:\GFramework\Process.h"
#include "F:\GFramework\Thread.h"
#include "F:\GFramework\Window.h"
#include "F:\GFramework\Handle.h"
#include "F:\GFramework\Service.h"
#include "F:\GFramework\NT.h"
#include "F:\GFramework\DriverInterface.h"

using namespace GF;

#include "Applications.h"
#include "Processes.h"
#include "Threads.h"
#include "Handles.h"
#include "Services.h"
#include "Options.h"

namespace Taskmanager {

	enum {
		ID_FRAME,
		ID_STATUSBAR,
		ID_NOTEBOOK,
		ID_PANEL_APP,
		ID_PANEL_PROC,
		ID_PANEL_THREAD,
		ID_PANEL_HANDLES,
		ID_PANEL_SERV,
		ID_PANEL_PERF,
		ID_PANEL_NET,
		ID_PANEL_USER,
		ID_PANEL_OPT
	};

	enum {
		ID_NOTEBOOK_APP,
		ID_NOTEBOOK_PROC,
		ID_NOTEBOOK_THREAD,
		ID_NOTEBOOK_HANDLE,
		ID_NOTEBOOK_SERV
	};

	struct Panel {
		wxPanel *applications, *processes, *threads, *handles, *services, *performance, *network, *user, *options;
		wxNotebook *notebook;
		wxWindow *main;
	}; extern Panel panel;

	class Taskmanager : public wxFrame {
	public:
		Taskmanager();
		~Taskmanager();

		void OnNotebookSelection(wxBookCtrlEvent &event);
	private:
	};

}

#endif