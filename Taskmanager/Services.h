#ifndef __SERVICE__
#define __SERVICE__

#include "Taskmanager.h"

namespace Services {

	enum {
		ID_PANEL,
		ID_LISTCTRL,
		ID_SEARCHCTRL,
		ID_CHECKBOX_KERNEL,
		ID_CHECKBOX_FILE,
		ID_CHECKBOX_OWN,
		ID_CHECKBOX_SHARE,
		ID_CONTEXTMENU_START,
		ID_CONTEXTMENU_STOP,
		ID_CONTEXTMENU_UNREGISTER,
		ID_CONTEXTMENU_OPENPATH,
	};

	class Services : public wxPanel {
	public:
		Services(wxWindow *win, wxWindowID winid);
		~Services();

		void ShowContextMenu();

		void Updates();
		void AddEntries(const ServiceList &sl);
		void RemoveEntries(const ServiceList &sl);
		void UpdateEntries();
		void UpdateListCtrl(const ServiceList &sl);

		void OnButtonEvent(wxCommandEvent &event);
		void OnCheckBoxEvent(wxCommandEvent &event);
		void OnContextMenuEvent(wxCommandEvent &event);
		void OnSearchCtrlEvent(wxCommandEvent &event);
		void OnColumnClickEvent(wxListEvent &event);

		static DWORD WINAPI TimerThread(LPVOID);
	private:
	};
}

#endif