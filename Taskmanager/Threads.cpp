#include "Threads.h"

namespace Threads {

	wxListCtrl *listCtrl;
	wxSearchCtrl *searchCtrl;
	wxImageList *imageList;

	bool all_users;
	ThreadEnum *pte;
	HANDLE handle_timer = NULL;

	Threads::Threads(wxWindow *win, wxWindowID winid) : wxPanel(win, winid)
	{
		pte = new ThreadEnum;
		all_users = false;

		imageList = new wxImageList(16, 16);

		listCtrl = new wxListCtrl(this, ID_LISTCTRL, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_VRULES);

		int c1 = listCtrl->InsertColumn(0, wxT("Thread Id"));
		int c2 = listCtrl->InsertColumn(1, wxT("Process Name"));
		int c3 = listCtrl->InsertColumn(2, wxT("User"));
		int c4 = listCtrl->InsertColumn(3, wxT("CPU"), wxLIST_FORMAT_CENTRE);
		int c5 = listCtrl->InsertColumn(4, wxT("PID"), wxLIST_FORMAT_RIGHT);
		int c6 = listCtrl->InsertColumn(5, wxT("Start Address"), wxLIST_FORMAT_RIGHT);

		listCtrl->SetColumnWidth(c2, 250);
		listCtrl->SetColumnWidth(c3, 120);

		searchCtrl = new wxSearchCtrl(this, ID_SEARCHCTRL);
		searchCtrl->ShowSearchButton(false);
		searchCtrl->SetDescriptiveText(wxT("Filter..."));

		searchCtrl->Connect(ID_SEARCHCTRL, wxEVT_TEXT, wxCommandEventHandler(Threads::OnSearchCtrlEvent));

		wxCheckBox *c = new wxCheckBox(this, ID_CHECKBOX, wxT("Show all users"), wxDefaultPosition, wxSize(-1, 23));
		c->SetValue(all_users);

		listCtrl->Bind(wxEVT_CHAR, [](wxKeyEvent &event) {
			int key = event.GetKeyCode();

			if (key == 127) {
				wxContextMenuEvent event(wxEVT_CONTEXT_MENU, ID_CONTEXTMENU_TERMINATE);
				reinterpret_cast<Threads*>(Taskmanager::panel.threads)->OnContextMenuEvent(event);
			}
		});

		CONNECTCHECKBOX(this, ID_CHECKBOX, Threads::OnCheckBoxEvent);

		wxBoxSizer *main = new wxBoxSizer(wxVERTICAL);
		wxBoxSizer *bar = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *bar_left = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *bar_right = new wxBoxSizer(wxHORIZONTAL);

		bar_left->Add(searchCtrl, 1, wxLEFT | wxRIGHT, 5);
		bar_right->Add(c);

		bar->Add(bar_left, 50);
		bar->Add(bar_right, 1, wxRIGHT, 5);

		main->Add(listCtrl, 1, wxGROW | wxALL, 5);
		main->Add(bar);

		SetSizerAndFit(main);

		handle_timer = ThreadManager::CreateThread(TimerThread);

		listCtrl->Connect(ID_LISTCTRL, wxEVT_CONTEXT_MENU, wxCommandEventHandler(Threads::OnContextMenuEvent));

		listCtrl->Bind(wxEVT_SIZE, [](wxSizeEvent &event) {

			wxSize size = listCtrl->GetClientSize();
			float width = float(size.GetWidth()) / 100;

			listCtrl->Freeze();

			listCtrl->SetColumnWidth(0, width * 10);
			listCtrl->SetColumnWidth(1, width * 30);
			listCtrl->SetColumnWidth(2, width * 15);
			listCtrl->SetColumnWidth(3, width * 7);
			listCtrl->SetColumnWidth(4, width * 9);
			listCtrl->SetColumnWidth(5, width * 29);

			listCtrl->Thaw();
		});
	}

	Threads::~Threads()
	{
		TerminateThread(handle_timer, 0);
		CloseHandle(handle_timer);

		delete pte;
	}

	void Threads::ShowContextMenu()
	{
		wxMenu *m = new wxMenu();
		wxMenuItem *terminate = new wxMenuItem(m, ID_CONTEXTMENU_TERMINATE, wxT("Terminate"));
		wxMenuItem *suspend = new wxMenuItem(m, ID_CONTEXTMENU_SUSPEND, wxT("Suspend"));
		wxMenuItem *resume = new wxMenuItem(m, ID_CONTEXTMENU_RESUME, wxT("Resume"));

		m->Append(terminate);
		m->Append(suspend);
		m->Append(resume);

		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_TERMINATE, Threads::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_SUSPEND, Threads::OnContextMenuEvent);
		CONNECTMENUITEM(listCtrl, ID_CONTEXTMENU_RESUME, Threads::OnContextMenuEvent);

		PopupMenu(m);
	}

	void Threads::Updates()
	{
		pte->Clear();

		ThreadList updated = pte->Updated();
		if (!pte->HasUpdated(updated))
			return;

		ThreadManager::FreeList(updated);

		UpdateListCtrl(pte->List());
	}

	void Threads::AddEntries(ThreadList ptl)
	{
		ProcessList list = ProcessManager::Enumerate();

		Process *p = ProcessManager::GetByProcessId(ProcessManager::GetCurrentProcessId(), &list, false);
		if (!p)
			return;

		wxString filter = searchCtrl->GetValue();

		for (auto &it : ptl) {

			Process *process = ProcessManager::GetByProcessId(it->ProcessId(), &list, false);
			if (!process)
				continue;

			wxString name = process->Name();
			wxString user = process->User();
			wxString pid = wxString::Format(wxT("%d"), it->ProcessId());
			wxString tid = wxString::Format(wxT("%d"), it->ThreadId());
			wxString start_address = wxString::Format(wxT("%p"), it->StartAddress());
			wxString cpu = wxString::Format(wxT("%.2f"), it->UsedCPU());

			if (process->Wow64())
				name.Append(wxT("*"));

			if (user.length() == 0)
				user.Append(wxT("UNKNOWN"));


			if (!all_users)
				if (process->User() != p->User())
					continue;

			wxString strname = name;
			filter.MakeLower();
			strname.MakeLower();

			if (filter.length())
				if (!strname.Contains(filter))
					continue;

			int image_index = 0;
			HICON handle_icon = ProcessManager::GetAppIcon(it->ProcessId());
			if (handle_icon) {
				wxIcon icon;
				icon.SetHICON(handle_icon);
				image_index = imageList->Add(icon);
				DestroyIcon(handle_icon);
			}
			else {
				image_index = imageList->Add(wxIcon(wxT("APPICON")));
			}

			int i = listCtrl->InsertItem(0, tid, image_index);

			listCtrl->SetItem(i, 1, name);
			listCtrl->SetItem(i, 2, user);
			listCtrl->SetItem(i, 3, cpu);
			listCtrl->SetItem(i, 4, pid);
			listCtrl->SetItem(i, 5, start_address);

			listCtrl->SetItemData(i, it->ThreadId());

			wxIdleEvent idle;
			listCtrl->SendIdleEvents(idle);
		}

		listCtrl->SetImageList(imageList, wxIMAGE_LIST_SMALL);

		ProcessManager::FreeList(list);
	}

	void Threads::RemoveEntries(ThreadList ptl)
	{
		for (auto &it : ptl) {
			LOOP_LISTCTRL_DONTCARE({
				DWORD tid = (DWORD)listCtrl->GetItemData(i);

				if (tid == it->ThreadId())
					listCtrl->DeleteItem(i);
			});
		}
	}

	void Threads::UpdateListCtrl(ThreadList ptl)
	{
		//listCtrl->Freeze();
		listCtrl->DeleteAllItems();

		AddEntries(ptl);

		//listCtrl->Thaw();
	}

	void Threads::OnButtonEvent(wxCommandEvent &event)
	{
		int id = event.GetId();
	}

	void Threads::OnCheckBoxEvent(wxCommandEvent &event)
	{
		int id = event.GetId();

		if (id == ID_CHECKBOX) {
			all_users = event.IsChecked();

			UpdateListCtrl(pte->List());
		}
	}

	void Threads::OnContextMenuEvent(wxCommandEvent &event)
	{
		int id = event.GetId();

		if (id == ID_LISTCTRL)
			if (LISTITEM_SELECTED() != -1)
				ShowContextMenu();

		std::list<int> remove_ids;

		LOOP_LISTCTRL_SELECTED({
			DWORD tid = (DWORD)listCtrl->GetItemData(i);

			if (id == ID_CONTEXTMENU_TERMINATE) {
				if (!ThreadManager::Terminate(tid))
					wxMessageBox(wxT("Could not terminate thread\n\nThread may be protected."), wxT("Error"), wxICON_EXCLAMATION);
				else
					remove_ids.push_back(i);
			}

			if (id == ID_CONTEXTMENU_SUSPEND)
				if (!ThreadManager::Suspend(tid))
					wxMessageBox(wxT("Could not suspend thread.\n\nThread may be protected or was already suspended."), wxT("Error"), wxICON_EXCLAMATION);

			if (id == ID_CONTEXTMENU_RESUME)
				if (!ThreadManager::Resume(tid))
					wxMessageBox(wxT("Could not resume thread.\n\nThread may be protected or was already resumed."), wxT("Error"), wxICON_EXCLAMATION);
		});

		for (auto &it : remove_ids)
			listCtrl->DeleteItem(it);
	}

	void Threads::OnSearchCtrlEvent(wxCommandEvent &event)
	{
		int id = event.GetId();

		searchCtrl->ShowCancelButton(searchCtrl->GetValue().length() != 0);

		if (id == ID_SEARCHCTRL)
			UpdateListCtrl(pte->List());
	}

	DWORD WINAPI Threads::TimerThread(LPVOID)
	{
		for (;;) {
			if (Taskmanager::panel.notebook->GetSelection() == 2) {
				ThreadList added = pte->Added();
				ThreadList removed = pte->Removed();

				if (pte->HasUpdated(added)) 
					reinterpret_cast<Threads*>(Taskmanager::panel.threads)->AddEntries(added);

				if (pte->HasUpdated(removed))
					reinterpret_cast<Threads*>(Taskmanager::panel.threads)->RemoveEntries(removed);

				ThreadManager::FreeList(added);
				ThreadManager::FreeList(removed);
			}

			Sleep(TASKMGR_REFRESH_INTERVAL);
		}
	}

}