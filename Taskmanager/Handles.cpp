#include "Handles.h"

namespace Handles {

	HandleEnum *he;
	//4 29 31 34
	wchar_t *type_name[] = { L"", L"", L"", L"Directory", L"", L"Token", L"Job", L"Process", L"Thread", L"UserApcReserve", L"", L"", L"Event", L"", L"Mutant", L"", L"Semaphore",
		L"Timer", L"", L"KeyedEvent", L"WindowStation", L"Desktop", L"TpWorkerFactory", L"", L"", L"", L"", L"IoCompletion", L"File", L"", L"", L"", L"",
		L"Section", L"", L"Key", L"ALPC Port", L"", L"", L"Unknown" };

	wxListCtrl *listCtrl;
	wxSearchCtrl *searchCtrl;
	wxImageList *imageList;
	wxCheckBox *cb1, *cb2, *cb3, *cb4, *cb5;

	bool driver_running = false;

	bool filter_file = false;
	bool filter_process = false;
	bool filter_thread = true;
	bool filter_key = true;
	bool filter_other = true;

	HANDLE handle_timer = NULL;

	Handles::Handles(wxWindow *win, wxWindowID winid) : wxPanel(win, winid)
	{
		he = new HandleEnum;

		imageList = new wxImageList(16, 16);

		listCtrl = new wxListCtrl(this, ID_LISTCTRL, wxDefaultPosition, wxDefaultSize, wxLC_REPORT /*| wxLC_SORT_ASCENDING*/ | wxLC_VRULES);

		int c1 = listCtrl->InsertColumn(0, wxT("Process"));
		int c2 = listCtrl->InsertColumn(1, wxT("PID"), wxLIST_FORMAT_RIGHT);
		int c3 = listCtrl->InsertColumn(2, wxT("Handle"), wxLIST_FORMAT_RIGHT);
		int c4 = listCtrl->InsertColumn(3, wxT("Type"));
		int c5 = listCtrl->InsertColumn(4, wxT("Value"));

		listCtrl->SetColumnWidth(c1, 150);
		listCtrl->SetColumnWidth(c2, 75);
		listCtrl->SetColumnWidth(c3, 60);
		listCtrl->SetColumnWidth(c4, 85);
		listCtrl->SetColumnWidth(c5, 375);

		searchCtrl = new wxSearchCtrl(this, ID_SEARCHCTRL);
		searchCtrl->ShowSearchButton(false);
		searchCtrl->SetDescriptiveText(wxT("Filter..."));

		searchCtrl->Connect(ID_SEARCHCTRL, wxEVT_TEXT, wxCommandEventHandler(Handles::OnSearchCtrlEvent));
		searchCtrl->SetFocus();

		cb1 = new wxCheckBox(this, ID_CHECKBOX_FILE, wxT("F"), wxDefaultPosition, wxSize(-1, 23));
		cb2 = new wxCheckBox(this, ID_CHECKBOX_PROCESS, wxT("P"), wxDefaultPosition, wxSize(-1, 23));
		cb3 = new wxCheckBox(this, ID_CHECKBOX_THREAD, wxT("T"), wxDefaultPosition, wxSize(-1, 23));
		cb4 = new wxCheckBox(this, ID_CHECKBOX_KEY, wxT("K"), wxDefaultPosition, wxSize(-1, 23));
		cb5 = new wxCheckBox(this, ID_CHECKBOX_OTHER, wxT("O"), wxDefaultPosition, wxSize(-1, 23));

		cb1->SetToolTip("File handles");
		cb2->SetToolTip("Process handles");
		cb3->SetToolTip("Thread handles");
		cb4->SetToolTip("Key handles");
		cb5->SetToolTip("Other handles");

		cb1->SetValue(!filter_file);
		cb2->SetValue(!filter_process);
		cb3->SetValue(!filter_thread);
		cb4->SetValue(!filter_key);
		cb5->SetValue(!filter_other);

		CONNECTCHECKBOX(this, ID_CHECKBOX_FILE, Handles::OnCheckBoxEvent);
		CONNECTCHECKBOX(this, ID_CHECKBOX_PROCESS, Handles::OnCheckBoxEvent);
		CONNECTCHECKBOX(this, ID_CHECKBOX_THREAD, Handles::OnCheckBoxEvent);
		CONNECTCHECKBOX(this, ID_CHECKBOX_KEY, Handles::OnCheckBoxEvent);
		CONNECTCHECKBOX(this, ID_CHECKBOX_OTHER, Handles::OnCheckBoxEvent);

		wxBoxSizer *main = new wxBoxSizer(wxVERTICAL);
		wxBoxSizer *bar = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *bar_left = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *bar_right = new wxBoxSizer(wxHORIZONTAL);

		bar_left->Add(searchCtrl, 1, wxLEFT | wxRIGHT, 5);
		bar_right->Add(cb1);
		bar_right->Add(cb2);
		bar_right->Add(cb3);
		bar_right->Add(cb4);
		bar_right->Add(cb5);

		bar->Add(bar_left, 50);
		bar->Add(bar_right, 1, wxRIGHT, 5);

		main->Add(listCtrl, 1, wxGROW | wxALL, 5);
		main->Add(bar);

		SetSizerAndFit(main);

		handle_timer = ThreadManager::CreateThread(TimerThread);

		listCtrl->Connect(ID_LISTCTRL, wxEVT_CONTEXT_MENU, wxCommandEventHandler(Handles::OnContextMenuEvent));

		listCtrl->Bind(wxEVT_SIZE, [](wxSizeEvent &event) {

			wxSize size = listCtrl->GetClientSize();
			float width = float(size.GetWidth()) / 100;

			listCtrl->Freeze();

			listCtrl->SetColumnWidth(0, width * 20);
			listCtrl->SetColumnWidth(1, width * 8);
			listCtrl->SetColumnWidth(2, width * 10);
			listCtrl->SetColumnWidth(3, width * 12);
			listCtrl->SetColumnWidth(4, width * 49);

			listCtrl->Thaw();
		});
	}

	Handles::~Handles()
	{
		TerminateThread(handle_timer, 0);
		CloseHandle(handle_timer);

		delete he;
	}

	void Handles::ShowContextMenu()
	{

	}


	void Handles::Updates()
	{
		he->Clear();

		HandleList updated = he->Updated();

		if (!he->HasUpdated(updated))
			return;

		HandleManager::FreeList(updated);

		UpdateListCtrl(he->List());
	}

	void Handles::AddEntries(HandleList hl)
	{
		wxString filter = searchCtrl->GetValue();

		ProcessList list = ProcessManager::Enumerate();

		for (auto &it : hl) {
			if (it->ProcessId() == GetCurrentProcessId())
				continue;

			if (it->Type() < 3 && it->Type() > 42)
				continue;

			Process *p = ProcessManager::GetByProcessId(it->ProcessId(), &list, false);

			int image_index = 0;
			HICON handle_icon = ProcessManager::GetAppIcon(it->ProcessId());
			if (handle_icon) {
				wxIcon icon;
				icon.SetHICON(handle_icon);
				image_index = imageList->Add(icon);
				DestroyIcon(handle_icon);
			}
			else {
				image_index = imageList->Add(wxIcon(wxT("APPICON")));
			}

			wxString strname = p->Name();
			wxString strvalue = it->Value();
			strname.MakeLower();
			strvalue.MakeLower();
			filter.MakeLower();

			if (filter.length())
				if (!strname.Contains(filter))
					if (!strvalue.Contains(filter))
						continue;

			wxString value = it->Value();
			if (value.length() == 0)
				continue;

			int i = listCtrl->InsertItem(0, p->Name().c_str(), image_index);

			listCtrl->SetItem(i, 1, wxString::Format("%d", it->ProcessId()));
			listCtrl->SetItem(i, 2, wxString::Format("%04X", it->Handle_()));
			listCtrl->SetItem(i, 3, it->Typename());
			listCtrl->SetItem(i, 4, it->Value());

			//TODO: fix this. handle value is not unique. however, it's unique in each process
			listCtrl->SetItemData(i, it->Handle_());

			wxIdleEvent idle;
			listCtrl->SendIdleEvents(idle);
		}

		ProcessManager::FreeList(list);

		listCtrl->SetImageList(imageList, wxIMAGE_LIST_SMALL);
	}

	void Handles::RemoveEntries(HandleList hl)
	{
		for (auto &it : hl) {
			LOOP_LISTCTRL_DONTCARE({
				DWORD handle = (DWORD)listCtrl->GetItemData(i);

				if (handle == it->Handle_())
					listCtrl->DeleteItem(i);
			}
		});
	}

	void Handles::UpdateEntries()
	{

	}

	void Handles::UpdateListCtrl(HandleList hl)
	{
		//listCtrl->Freeze();
		listCtrl->DeleteAllItems();

		AddEntries(hl);

		//listCtrl->Thaw();
	}

	void Handles::OnButtonEvent(wxCommandEvent &event)
	{

	}

	void Handles::OnCheckBoxEvent(wxCommandEvent &event)
	{

	}

	void Handles::OnContextMenuEvent(wxCommandEvent &event)
	{
		
	}

	void Handles::OnSearchCtrlEvent(wxCommandEvent &event)
	{
		int id = event.GetId();

		searchCtrl->ShowCancelButton(searchCtrl->GetValue().length() != 0);

		if (id == ID_SEARCHCTRL)
			UpdateListCtrl(he->List());
	}

	DWORD WINAPI Handles::TimerThread(LPVOID)
	{
		for (;;) {
			if (Taskmanager::panel.notebook->GetSelection() == 3) {
				HandleList added = he->Added();
				HandleList removed = he->Removed();

				if (he->HasUpdated(added))
					reinterpret_cast<Handles*>(Taskmanager::panel.handles)->AddEntries(added);

				if (he->HasUpdated(removed))
					reinterpret_cast<Handles*>(Taskmanager::panel.handles)->RemoveEntries(removed);

				HandleManager::FreeList(added);
				HandleManager::FreeList(removed);

				reinterpret_cast<Handles*>(Taskmanager::panel.handles)->UpdateEntries();
			}

			Sleep(TASKMGR_REFRESH_INTERVAL);
		}
	}

}