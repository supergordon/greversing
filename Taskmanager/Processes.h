#ifndef __PROCESSES__
#define __PROCESSES__

#include "Taskmanager.h"

namespace Processes {

	enum {
		ID_PANEL,
		ID_LISTCTRL,
		ID_SEARCHCTRL,
		ID_CHECKBOX,
		ID_CONTEXTMENU_OPENPATH,
		ID_CONTEXTMENU_RESTART,
		ID_CONTEXTMENU_TERMINATE,
		ID_CONTEXTMENU_SUSPEND,
		ID_CONTEXTMENU_RESUME,
		ID_CONTEXTMENU_SHOWHANDLES,
		ID_CONTEXTMENU_SHOWTHREADS,
		ID_CONTEXTMENU_HIDE,
		ID_CONTEXTMENU_PROTECT,
		ID_CONTEXTMENU_DUMP,
		ID_CONTEXTMENU_PROPERTIES,
	};

	class Processes : public wxPanel {
	public:
		Processes(wxWindow *win, wxWindowID winid);
		~Processes();

		void ShowContextMenu();

		void Updates();
		void AddEntries(const ProcessList &pl);
		void RemoveEntries(const ProcessList &pl);
		void UpdateEntries();
		void UpdateListCtrl(const ProcessList &pl);

		void OnButtonEvent(wxCommandEvent &event);
		void OnCheckBoxEvent(wxCommandEvent &event);
		void OnContextMenuEvent(wxCommandEvent &event);
		void OnSearchCtrlEvent(wxCommandEvent &event);
		void OnColumnClickEvent(wxListEvent &event);

		static DWORD WINAPI TimerThread(LPVOID);
	private:
	};

}

#endif