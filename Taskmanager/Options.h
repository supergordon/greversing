#ifndef __OPTIONS__
#define __OPTIONS__

#include "Taskmanager.h"

namespace Options {

	enum {
		ID_PANEL
	};

	class Options : public wxPanel {
	public:
		Options(wxWindow *win, wxWindowID winid);
		~Options();
	private:
	};

}

#endif