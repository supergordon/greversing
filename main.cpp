/***********************************************************************

	Application Name:	Gordon Reversing - Tool
	Creation Date:		06.04.2014 

	Features: (TODO!)
				Advanced task selector + manager
				Memory Searcher
				Disassembler
				Debugger
				DLL Injector
				PE Viewer + Editor

************************************************************************/

#include "main.h"
#include "MainDialog.h"
#include "vars.h"
#include "MemorySearcher\MemorySearcher.h"
#include "Taskmanager\Taskmanager.h"
#include "F:\GFramework\Process.h"
#include "F:\GFramework\Window.h"

IMPLEMENT_APP(AppInit)


bool AppInit::OnInit()
{
	if(!ProcessManager::RunningAsAdmin(GetCurrentProcessId())) {
		int ret = wxMessageBox("This application must be running as administrator to function properly.\n\nNow running with restricted access.", "Warning", wxICON_WARNING);
	}

	ProcessManager::SetDebugPrivilege();
    //MainDialog* mainDialog = new MainDialog(wxT(APP_NAME));
    //mainDialog->Show(true);

	//Taskmanager::Taskmanager* taskmgr = new Taskmanager::Taskmanager();
	//taskmgr->Show();

	MemorySearcher::MemorySearcher *memsearcher = new MemorySearcher::MemorySearcher();
	memsearcher->Show();

	return true;
}