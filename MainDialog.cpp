#include "MainDialog.h"
#include "Taskmanager\Taskmanager.h"

enum {
	MENU_FILE_CLOSE
};

MainDialog::MainDialog(const wxString& title)
       : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(800, 600))
{

	/* Men�bar */
	wxMenuBar* menubar_main = new wxMenuBar();
	wxMenu* menu_file = new wxMenu();
	wxMenuItem* menuitem_close = new wxMenuItem(menu_file, MENU_FILE_CLOSE,	wxT("Close"));

	menu_file->Append(menuitem_close);
	menubar_main->Append(menu_file, wxT("File"));

	SetMenuBar(menubar_main);
//	CONNECTMENUITEM(this, MENU_FILE_CLOSE, MainDialog::On_Menu_File_Close);

	/* Bitmaps */
	wxImage::AddHandler( new wxPNGHandler );
	wxBitmap exit(wxT("g.png"), wxBITMAP_TYPE_PNG);

	/* Toolbar */
	wxToolBarBase* toolbar = CreateToolBar(wxNO_BORDER | wxHORIZONTAL | wxTB_TEXT);
	toolbar->AddTool(0, wxT("gay"), exit);
	toolbar->Realize();

	CreateStatusBar(3);


	

	this->Center();
}
void MainDialog::On_Menu_File_Close(wxCommandEvent& event)
{
	this->Close();
}