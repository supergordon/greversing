#ifndef _MAIN_
#define _MAIN_

#include <wx/wx.h>
#include <wx/image.h>
//#include <wx/textctrl.h>
#include <wx/notebook.h>
#include <wx/spinctrl.h>
#include <wx/listctrl.h>
#include <wx/statline.h>
#include <wx/string.h>
#include <wx/toolbar.h>

#include <windows.h>
#include <vector>


class MainDialog : public wxFrame
{
public:
    MainDialog(const wxString& title);

	void On_Menu_File_Close(wxCommandEvent& event);

private:

};

#endif