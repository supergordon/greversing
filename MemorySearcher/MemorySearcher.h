#ifndef __MEMORYSEARCHER__
#define __MEMORYSEARCHER__

#pragma warning (disable: 4996)

#include "..\vars.h"
#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/srchctrl.h>
#include <wx/event.h>
#include <wx/notebook.h>
#include <wx/choicebk.h>
#include <wx/icon.h>

#include "F:\GFramework\Process.h"
#include "F:\GFramework\Thread.h"
#include "F:\GFramework\Window.h"
#include "F:\GFramework\Handle.h"
#include "F:\GFramework\Service.h"
#include "F:\GFramework\NT.h"
#include "F:\GFramework\DriverInterface.h"

using namespace GF;

namespace MemorySearcher {

	enum {
		ID_FRAME,
		ID_PANEL,
		ID_DIALOG_CHOOSE,
		ID_LISTCTRL_RESULT,
		ID_LISTCTRL_EDIT,
		ID_LABEL_FOUND,
		ID_BUTTON_FIRST,
		ID_BUTTON_NEXT,
		ID_BUTTON_EDIT,
		ID_BITMAP_ICON,
		ID_TEXTCTRL,
		ID_COMBOBOX_SCAN,
		ID_COMBOBOX_TYPE,
		ID_CHECKBOX_PAUSE,
		ID_CHECKBOX_SPEEDHACK,
	};

	extern wxFont font;

	class MemorySearcher : public wxFrame {
	public:
		MemorySearcher();
		~MemorySearcher();

		void OnChooseTarget(wxCommandEvent &event);
	private:
	};

}

#endif