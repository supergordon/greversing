#include "MemorySearcher.h"

namespace MemorySearcher {

	wxFrame *frame = NULL;
	wxButton *buttonEdit = NULL;
	wxListCtrl *listCtrlEdit = NULL;
	wxListCtrl *listCtrlResult = NULL;
	wxBitmapButton *bitmapButton = NULL;
	wxPanel *panel = NULL;
	wxPanel *panel_left = NULL;
	wxPanel *panel_right = NULL;

	wxDialog *dialog = NULL;
	wxChoicebook *choiceBook = NULL;
	wxPanel *panelProcesses = NULL;
	wxPanel *panelWindows = NULL;
	wxListCtrl *listCtrlProcesses = NULL;
	wxListCtrl *listCtrlWindows = NULL;

	wxFont font(9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, _G("Calibri"));

	MemorySearcher::MemorySearcher() : wxFrame(NULL, ID_FRAME, wxT(W_NAME_MEMSEARCHER), wxDefaultPosition, wxSize(500, 400))
	{
		frame = reinterpret_cast<wxFrame*>(FindWindowById(ID_FRAME, this));

		SetFont(font);
		wxString name = this->GetTitle();

#if defined (_M_X64)
		name.Append(wxT(" (x64 version)"));
#else
		name.Append(wxT(" (x86 version)"));
#endif

		this->SetTitle(name);

		wxInitAllImageHandlers();
		SetIcon(wxIcon(wxT("APPICON")));

		panel = new wxPanel(this, ID_PANEL);
		panel_left = new wxPanel(panel, wxID_ANY);
		panel_right = new wxPanel(panel, wxID_ANY);

		listCtrlEdit = new wxListCtrl(panel, ID_LISTCTRL_EDIT, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_SORT_ASCENDING | wxLC_VRULES);
		listCtrlResult = new wxListCtrl(panel_right, ID_LISTCTRL_RESULT, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_SORT_ASCENDING | wxLC_VRULES);

		listCtrlEdit->SetMaxSize(wxSize(-1, 300));
		listCtrlResult->SetMinSize(wxSize(190, -1));

		bitmapButton = new wxBitmapButton(panel_left, ID_BITMAP_ICON, wxBitmap(wxIcon(wxT("APPICON"))), wxDefaultPosition, wxSize(35, -1), wxBORDER_NONE);
		bitmapButton->Connect(wxEVT_BUTTON, wxCommandEventHandler(MemorySearcher::OnChooseTarget));
		bitmapButton->SetFocus();

		wxString arrValue[] = { "Exact value", "Bigger than...", "Smaller than...", "Increased", "Decreased", "Value between", "Unknown"};
		wxArrayString arrayValueType(sizeof(arrValue) / sizeof(wxString), arrValue);

		wxString arrType[] = { "1-Byte", "2-Byte", "4-Byte", "8-Byte", "Float", "Double", "Text", "Array" };
		wxArrayString arrayScanType(sizeof(arrType) / sizeof(wxString), arrType);

		wxButton *buttonFirst = new wxButton(panel_left, ID_BUTTON_FIRST, "First Scan");
		wxButton *buttonNext = new wxButton(panel_left, ID_BUTTON_NEXT, "Next Scan");
		wxStaticText *labelFound = new wxStaticText(panel_right, ID_LABEL_FOUND, "Found: 0");
		wxStaticText *labelValue = new wxStaticText(panel_left, wxID_ANY, "Value", wxDefaultPosition, wxSize(35, -1));
		wxStaticText *labelScan = new wxStaticText(panel_left, wxID_ANY, "Scan", wxDefaultPosition, wxSize(35, -1));
		wxStaticText *labelType = new wxStaticText(panel_left, wxID_ANY, "Type", wxDefaultPosition, wxSize(35, -1));
		wxTextCtrl *textValue = new wxTextCtrl(panel_left, ID_TEXTCTRL);
		wxComboBox *comboBoxScan = new wxComboBox(panel_left, ID_COMBOBOX_SCAN, arrValue[0], wxDefaultPosition, wxDefaultSize, arrayValueType, wxCB_READONLY);
		wxComboBox *comboBoxType = new wxComboBox(panel_left, ID_COMBOBOX_TYPE, arrType[0], wxDefaultPosition, wxDefaultSize, arrayScanType, wxCB_READONLY);
		wxCheckBox *checkBoxPause = new wxCheckBox(panel_left, ID_CHECKBOX_PAUSE, wxT("Pause while scanning"));
		wxCheckBox *checkBoxSpeed = new wxCheckBox(panel_left, ID_CHECKBOX_SPEEDHACK, wxT("Speedhack"));
		buttonEdit = new wxButton(panel_right, wxID_ANY, wxT("v"), wxDefaultPosition, wxSize(14, 14));

		buttonEdit->SetToolTip("Toggle minimal user interface");
		bitmapButton->SetToolTip("Pick a target");
		labelFound->SetToolTip("Number of found values in last search");

		int c1 = listCtrlResult->InsertColumn(0, wxT("Address"));
		int c2 = listCtrlResult->InsertColumn(1, wxT("Value"), wxLIST_FORMAT_RIGHT);

		int d1 = listCtrlEdit->InsertColumn(0, wxT("Frozen"));
		int d2 = listCtrlEdit->InsertColumn(1, wxT("Description"));
		int d3 = listCtrlEdit->InsertColumn(0, wxT("Address"));
		int d4 = listCtrlEdit->InsertColumn(0, wxT("Type"));
		int d5 = listCtrlEdit->InsertColumn(0, wxT("Value"), wxLIST_FORMAT_RIGHT);

		buttonEdit->Bind(wxEVT_BUTTON, [](wxCommandEvent &event) {
			static bool toggle = false;
			toggle = !toggle;
			buttonEdit->SetLabel(toggle ? "^" : "v");
			listCtrlEdit->Show(toggle);

			panel->Fit();
			frame->SetClientSize(panel->GetSize());
			frame->SetSize(panel->GetSize());
		});

		wxBoxSizer *main = new wxBoxSizer(wxVERTICAL);
		wxBoxSizer *up_middle = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *left = new wxBoxSizer(wxVERTICAL);
		wxBoxSizer *right = new wxBoxSizer(wxVERTICAL);
		wxBoxSizer *right_1 = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *left_1 = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *left_2 = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *left_3 = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *left_4 = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *left_5 = new wxBoxSizer(wxHORIZONTAL);

		left_1->Add(bitmapButton, 0, wxALIGN_CENTER);
		left_1->Add(buttonFirst, 1, wxLEFT | wxRIGHT | wxALIGN_CENTER, 5);
		left_1->Add(buttonNext, 1, wxALIGN_CENTER);

		left_2->Add(labelValue, 0, wxRIGHT | wxALIGN_CENTER, 5);
		left_2->Add(textValue, 1, wxALIGN_CENTER);

		left_3->Add(labelScan, 0, wxRIGHT | wxALIGN_CENTER, 5);
		left_3->Add(comboBoxScan, 1, wxALIGN_CENTER);

		left_4->Add(labelType, 0, wxRIGHT | wxALIGN_CENTER, 5);
		left_4->Add(comboBoxType, 1, wxALIGN_CENTER);

		left_5->Add(checkBoxSpeed, 1, wxRIGHT | wxALIGN_CENTER, 5);
		left_5->Add(checkBoxPause, 1, wxALIGN_CENTER);

		left->Add(left_1, 0, wxEXPAND | wxLEFT | wxTOP | wxBOTTOM, 5);
		left->Add(left_2, 0, wxEXPAND | wxLEFT | wxTOP | wxBOTTOM, 5);
		left->Add(left_3, 0, wxEXPAND | wxLEFT | wxTOP | wxBOTTOM, 5);
		left->Add(left_4, 0, wxEXPAND | wxLEFT | wxTOP | wxBOTTOM, 5);
		//left->AddStretchSpacer();
		left->Add(left_5, 0, wxEXPAND | wxLEFT | wxTOP | wxBOTTOM, 5);

		right_1->Add(labelFound);
		right_1->AddStretchSpacer();
		right_1->Add(buttonEdit);

		right->Add(right_1, 0, wxEXPAND | wxLEFT | wxTOP | wxBOTTOM, 5);
		right->Add(listCtrlResult, 1, wxEXPAND);

		panel_left->SetMaxSize(wxSize(250, 250));

		panel_left->SetSizerAndFit(left);
		panel_right->SetSizerAndFit(right);

		up_middle->Add(panel_left, 0, wxEXPAND | wxALL, 5);
		up_middle->Add(panel_right, 3, wxEXPAND | wxALL, 5);

		main->Add(up_middle, 3, wxEXPAND);
		main->Add(listCtrlEdit, 2, wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM, 5);

		listCtrlEdit->Show(false);

		panel->SetSizerAndFit(main);

		wxSize size = GetClientSize();
		size.y -= listCtrlEdit->GetClientSize().y;

		SetMinSize(size);
		panel->SetMinSize(size);

		Fit();
	}

	MemorySearcher::~MemorySearcher()
	{

	}

	//TODO: quick filter
	void MemorySearcher::OnChooseTarget(wxCommandEvent &event)
	{
		dialog = new wxDialog(0, ID_DIALOG_CHOOSE, "Picker", wxDefaultPosition, wxSize(240, 340));
		choiceBook = new wxChoicebook(dialog, wxID_ANY, wxDefaultPosition);
		panelProcesses = new wxPanel(choiceBook);
		panelWindows = new wxPanel(choiceBook);
		listCtrlProcesses = new wxListCtrl(panelProcesses, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_SINGLE_SEL | wxLC_SORT_ASCENDING | wxLC_NO_HEADER | wxLC_VRULES | wxLC_HRULES);
		listCtrlWindows = new wxListCtrl(panelWindows, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_SINGLE_SEL | wxLC_SORT_ASCENDING | wxLC_NO_HEADER | wxLC_VRULES | wxLC_HRULES);

		choiceBook->AddPage(panelProcesses, "Processes", true);
		choiceBook->AddPage(panelWindows, "Windows", false);

		int column1 = listCtrlProcesses->InsertColumn(0, "Name");
		int column2 = listCtrlProcesses->InsertColumn(1, "PID", wxLIST_FORMAT_RIGHT);
		listCtrlProcesses->SetColumnWidth(column1, 160);
		listCtrlProcesses->SetColumnWidth(column2, 45);

		column1 = listCtrlWindows->InsertColumn(0, "Name");
		column2 = listCtrlWindows->InsertColumn(1, "PID", wxLIST_FORMAT_RIGHT);
		listCtrlWindows->SetColumnWidth(column1, 160);
		listCtrlWindows->SetColumnWidth(column2, 45);

		listCtrlProcesses->SetFocus();
		//::SetWindowLongPtr(dialog->GetHWND(), GWL_EXSTYLE, WS_EX_PALETTEWINDOW);

		wxImageList *imageList = new wxImageList(16, 16);

		{
			ProcessList list = ProcessManager::Enumerate();

			for (auto &it : list) {
				if (it->ProcessId() == 0)
					continue;

				int a = 0;
				HICON handle_icon = ProcessManager::GetAppIcon(it->ProcessId());

				if (handle_icon) {
					wxIcon icon;
					icon.SetHICON(handle_icon);
					a = imageList->Add(icon);
					DestroyIcon(handle_icon);
				}
				else {
					a = imageList->Add(wxIcon(wxT("APPICON")));
				}

				int i = listCtrlProcesses->InsertItem(0, it->Name().c_str(), a);
				listCtrlProcesses->SetItem(i, 1, wxString::Format("%d", it->ProcessId()));
				listCtrlProcesses->SetItemData(i, it->ProcessId());

				if (it->Wow64())
					listCtrlProcesses->SetItemBackgroundColour(i, wxColour(240, 240, 240));
			}

			listCtrlProcesses->SetImageList(imageList, wxIMAGE_LIST_SMALL);
			ProcessManager::FreeList(list);
		}

		{
			WindowList list = WindowManager::Enumerate();

			for (auto &it : list) {
				int a = 0;
				HICON handle_icon = it->Icon();

				if (handle_icon) {
					wxIcon icon;
					icon.SetHICON(handle_icon);
					a = imageList->Add(icon);
					DestroyIcon(handle_icon);
				}
				else {
					a = imageList->Add(wxIcon(wxT("APPICON")));
				}

				int i = listCtrlWindows->InsertItem(0, it->Title().c_str(), a);
				listCtrlWindows->SetItem(i, 1, wxString::Format("%d", it->ProcessId()));
				listCtrlWindows->SetItemData(i, it->ProcessId());

				if (it->Wow64())
					listCtrlWindows->SetItemBackgroundColour(i, wxColour(240, 240, 240));
			}

			listCtrlWindows->SetImageList(imageList, wxIMAGE_LIST_SMALL);
			WindowManager::FreeList(list);
		}

		listCtrlProcesses->Bind(wxEVT_COMMAND_LIST_ITEM_ACTIVATED, [](wxListEvent &event) {
			DWORD process_id = event.GetData();
			HICON ico = ProcessManager::GetAppIcon(process_id, ProcessManager::GICON_LARGE);

			wxIcon icon; icon.CreateFromHICON(ico);
			bitmapButton->SetBitmap(wxBitmap(icon));

			DestroyIcon(ico);
			dialog->Destroy();
		});

		listCtrlWindows->Bind(wxEVT_COMMAND_LIST_ITEM_ACTIVATED, [](wxListEvent &event) {
			DWORD process_id = event.GetData();
			HICON ico = ProcessManager::GetAppIcon(process_id, ProcessManager::GICON_LARGE);

			wxIcon icon; icon.CreateFromHICON(ico);
			bitmapButton->SetBitmap(wxBitmap(icon));

			DestroyIcon(ico);
			dialog->Destroy();
		});

		dialog->Bind(wxEVT_CHAR_HOOK, [](wxKeyEvent &event) {
			int key = event.GetKeyCode();
			int sel = choiceBook->GetSelection();

			if (key == VK_TAB) {
				choiceBook->SetSelection(!sel);
				sel ? listCtrlProcesses->SetFocus() : listCtrlWindows->SetFocus();
				return;
			}
			else if (key == VK_ESCAPE) {
				dialog->Destroy();
			}

			event.Skip();
		});

		choiceBook->Bind(wxEVT_CHOICE, [](wxCommandEvent &event) {
			int sel = choiceBook->GetSelection();
			sel ? listCtrlProcesses->SetFocus() : listCtrlWindows->SetFocus();
			event.Skip();
		});

		wxBoxSizer *boxSizer1 = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *boxSizer2 = new wxBoxSizer(wxHORIZONTAL);

		boxSizer1->Add(listCtrlProcesses, 1, wxEXPAND);
		boxSizer2->Add(listCtrlWindows, 1, wxEXPAND);

		panelProcesses->SetSizerAndFit(boxSizer1);
		panelWindows->SetSizerAndFit(boxSizer2);

		dialog->ShowModal();
		dialog->Destroy();
	}
}